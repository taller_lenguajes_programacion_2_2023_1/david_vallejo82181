import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserRegisterService {
  private apiUrl = 'http://localhost:9001/market'; // Reemplaza con la URL de tu backend

  constructor(private http: HttpClient) {}

  enviarDatos(formData: any) {
    return this.http.post(`${this.apiUrl}/users`, formData);
  }
  /*
  constructor() { }
  
  private async setUser(userInfo: any){
    //@ts-ignore
    const users = JSON.parse(sessionStorage.getItem('users')) || [];
    sessionStorage.setItem('users', JSON.stringify([...users, userInfo]))
  }
  
  async registerUser(userInfo: any){
    debugger
    await this.setUser(userInfo);
    // aqui vamos a hacer la petición
  }*/
}
