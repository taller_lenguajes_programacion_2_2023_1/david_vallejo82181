import { Component } from '@angular/core';


declare var require: any;

var myUser= require('src/assets/json/books.json');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'curso-politecnico';
  Libros: any = myUser;
}
