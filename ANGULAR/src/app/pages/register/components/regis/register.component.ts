import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UserRegisterService } from 'src/app/core/services/userRegister/user-register.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  public formModal: any;
  public errorService: string | undefined;
  public registerForm = new FormGroup({
    userName: new FormControl(),
    email:  new FormControl(),
    password:  new FormControl(),
    confirm:  new FormControl(),
  });
  
  constructor(
   private _userRegister : UserRegisterService,
   private router: Router
  ){}
  
  public submit(): void {
    
    /*
    if(this.isNew(JSON.stringify(this.registerForm.value.userName))){
      //let Aux = new Array();
      let Aux = JSON.parse(localStorage.getItem("Accounts")??'[]');
      //alert(JSON.stringify(Aux)+''+JSON.stringify(this.registerForm.value))
      Aux.push(this.registerForm.value)
      localStorage.setItem("Accounts",JSON.stringify(Aux))
      alert("Usuario Creado")
    }
    else{
      alert("El nombre de usuario ya esta registrado");
    }*/
    
    /*
    localStorage.setItem("persona",JSON.stringify(this.registerForm.value));
    const persona=this.registerForm.value
    const user=persona.userName
    const email=persona.email
    const pass=persona.password
    
    //if(persona.password === persona.confirm){const pass=persona.password}
    alert(user+email+pass+" usuario almacenado en LocalStorage")
    this.router.navigate(['/login']);
    */
    
    /*debugger
    console.log(this.registerForm.value)
    this._userRegister.registerUser(this.registerForm.value).then(()=> {
      debugger
      alert('Registro guardado!');
      this.registerForm.reset();
    })
    */
  }
  public isNew(Name:String): boolean {
    let Aux = JSON.parse(localStorage.getItem("Accounts")??'[]');
    for (const i of Aux){
      //alert(Name +' - '+JSON.stringify(i.userName))
      if(Name === JSON.stringify(i.userName)){
        return false;
      }
    }
    return true;
  }
}

