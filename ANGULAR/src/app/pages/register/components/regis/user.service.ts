import { Component } from '@angular/core';
import { UserRegisterService } from '../../../../core/services/userRegister/user-register.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class FormularioComponent {
  formData = {
    name: '',
    email: ''
  };

  constructor(private dataService: UserRegisterService) { }

  submitForm() {
    this.dataService.enviarDatos(this.formData)
      .subscribe(
        response => {
          console.log('Datos enviados correctamente', response);
          // Realiza alguna acción adicional después de enviar los datos, si es necesario
        },
        error => {
          console.error('Error al enviar los datos', error);
          // Maneja el error de forma adecuada
        }
      );
  }
}