import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/core/services/products/products.service';
import { Router } from '@angular/router';

declare var require: any;
var myUser= require('src/assets/json/books.json');

declare var window: any;

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit{
  public formModal: any;
  public errorService: string | undefined;
  public listProducts: any;
  public Libros: any = myUser;
  public userInfo:any={};

  constructor(
    private router: Router,
    private productsService: ProductsService) {
    
  }

  ngOnInit(): void {
    
    this.callProcuts();
    this.formModal = new window.bootstrap.Modal(
      document.getElementById('myModal')
    );  
  }
 
  public callProcuts(): void{
    this.productsService.getAllProducts().subscribe(
      data => {
        console.log("data: ",data)
        this.listProducts = data.products
      }
    );
  }
  public sendProduct(Index:any):void{
    const info=JSON.parse( sessionStorage.getItem("user")||"0")
    const value=JSON.stringify({"name":info.username,"Ind":Index})
    sessionStorage.setItem("Key",value)
    console.log(JSON.parse(sessionStorage.getItem("Key")||""))
    this.router.navigate(['/webapp/play'])
    //console.log(info)
    //this.userInfo=JSON.parse(info) || {};
    //const id=`card${Index}`
    /*
    console.log(id)
    const card= document.getElementById(id)
    console.log(card)
    */
    

  }

}
/*
<a *ngFor="let element of listProducts">
                        <div class="card" style="width: 18rem;">
                            <img class="card-img-top" src="{{element.thumbnail}}" alt="Card image cap">
                            <div class="card-body">
                              <h5 class="card-title">{{ element.title }}</h5>
                              <h6 class="card-author">{{ element.brand }}</h6>
                              <p class="card-text">{{ element.thumbnail }}</p>
                              <a href="#" class="btn btn-primary">Detalle</a>
                            </div>
                          </div>
                    </a>
*/
