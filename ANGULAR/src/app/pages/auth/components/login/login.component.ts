import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Login } from 'src/app/core/interfaces/login';
import { AuthService } from 'src/app/core/services/auth/auth.service';

declare var window: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public formModal: any;
  public errorService: string | undefined;

  public loginForm: FormGroup = new FormGroup({
    username: new FormControl(null, [
      Validators.required,
      Validators.pattern("^[a-zA-Z0-9]+$"),
      Validators.minLength(3),
      Validators.maxLength(20)]),
    password: new FormControl(null, [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(25)]),
  });

  public errorMessages = {
    'username': [
      { type: 'required', message: 'El campo es requerido.' },
      { type: 'pattern', message: 'El campo es numerico.' },
      { type: 'minlength', message: 'Minimo es de 3 caracteres.' },
      { type: 'maxlength', message: 'Maximo es de 15 caracteres.' },
    ],
    'password': [
      { type: 'required', message: 'El campo es requerido.' },
      { type: 'pattern', message: 'The password if minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character.' },
      { type: 'minlength', message: 'Minimo es de 5 caracteres.' },
      { type: 'maxlength', message: 'Maximo es de 25 caracteres.' },
    ]
  };

  constructor(
    private router: Router,//cambios de pagina
    private authService: AuthService) { }

  ngOnInit(): void {
    sessionStorage.clear();
    this.formModal = new window.bootstrap.Modal(
      document.getElementById('myModal')
    );  
  }

  public submit(): void {
    //debugger
    let login=this.loginForm.value
    if(this.exists(JSON.stringify(login.username),JSON.stringify(login.password))){
      this.router.navigate(['/dashboard'])
      sessionStorage.setItem("user",JSON.stringify(this.loginForm.value))
    }
    else{
      alert("Nombre y/o usuario incorrectos")
    }
    //alert(login)
    /*
    const persona=JSON.parse(localStorage.getItem("persona")||"melo")
    //alert(JSON.stringify(login)+JSON.stringify(persona))
    if(login.username === persona.userName && login.password === persona.password){
      //alert("melisimo")
      this.router.navigate(['/dashboard']);
    }
    else{
      alert("Nombre y/o usuario incorrectos")
    }
    */

    //alert(JSON.stringify(this.loginForm.value))
    
    /**/
    //debugger
    /*
    this.authService.auth(this.loginForm.value).subscribe(
      (res: any) => {
        //alert(JSON.parse(res))
        console.log(res);
        //alert(JSON.parse(res))        /NO FUNCIONA
        sessionStorage.setItem("userInfo",JSON.stringify(res)); //(nombre, ruta)
        let expireDate = new Date().getTime() + (1000 * res.expires_in);
        sessionStorage.setItem('token', res.token);
        this.router.navigate(['/dashboard']);
      },
      err => {
        console.log(err)
        this.errorService = err.error.message;  
        this.formModal.show();
      }
    );*/
    
  }
  public exists(Name:String,Pass:String): boolean {
    let Aux = JSON.parse(localStorage.getItem("Accounts")??'[]');
    for (const i of Aux){
      //alert(Name +' - '+JSON.stringify(i.userName)+' - '+Pass+' - '+JSON.stringify(i.password))
      if(Name === JSON.stringify(i.userName) && Pass === JSON.stringify(i.password)){
        return true;
      }
    }
    return false;
  }
}
