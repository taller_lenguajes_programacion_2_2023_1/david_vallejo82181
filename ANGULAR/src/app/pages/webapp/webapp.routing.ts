import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import { playComponent } from "./components/play/play.component";

const routes: Routes =[
    {
        path: 'play',
        component: playComponent
    }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports:[
    RouterModule
  ]
})
export class webappRouting { }
