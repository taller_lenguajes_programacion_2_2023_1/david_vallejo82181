import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { webappRouting } from './webapp.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { playComponent } from './components/play/play.component';

@NgModule({
  schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
  declarations: [
    playComponent
  ],
  imports: [
    CommonModule,
    webappRouting,
    SharedModule
  ]
})
export class webappModule { }
