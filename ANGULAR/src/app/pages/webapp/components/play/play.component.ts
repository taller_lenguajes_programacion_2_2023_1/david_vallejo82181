import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/core/services/products/products.service';
import { Router } from '@angular/router';

declare var window: any;
declare var require: any;
var myUser= require('src/assets/json/books.json');

@Component({
  selector: 'app-list',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class playComponent implements OnInit{
  public formModal: any;
  public errorService: string | undefined;
  public Libros: any = myUser;
  public token:any;

  constructor(
    private router: Router,
    private productsService: ProductsService) {
    
  }

  ngOnInit(): void {
    this.Auth();
  }
  public Auth():void{
    if(JSON.parse(sessionStorage.getItem("Key")||"").name === JSON.parse(sessionStorage.getItem("user")||"").username){
      this.token=JSON.parse(sessionStorage.getItem("Key")||"").Ind;
      console.log(this.token)
      /*
      console.log(this.Libros[0])
      //let Aux=JSON.parse(this.Libros)
      //console.log(Aux)
      for (const i of this.Libros){
        console.log(i.titulo)
      }
      */
    }
    else{
      alert("No tiene permiso de acceder, Autoricese o Registrese")
      this.router.navigate(['/auth'])
    }
  }
}
