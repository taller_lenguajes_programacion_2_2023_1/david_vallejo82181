package com.co.jic.template.market.controller;

import com.co.jic.template.market.DTO.BookDTO;
import com.co.jic.template.market.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:4200")
@RestController
public class BookController {
    private final BookService bookService;

    public BookController(BookService bookService){this.bookService=bookService;}
}
