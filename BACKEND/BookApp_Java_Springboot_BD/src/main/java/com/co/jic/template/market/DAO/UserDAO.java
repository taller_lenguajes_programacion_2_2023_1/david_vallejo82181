package com.co.jic.template.market.DAO;

import com.co.jic.template.market.domain.user.User;
import com.co.jic.template.market.domain.user.UserCode;
import com.co.jic.template.market.domain.user.UserName;
import com.co.jic.template.market.domain.user.UserMail;
import com.co.jic.template.market.domain.user.UserPass;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "users")
@Data
public class UserDAO {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String mail;

    @Column
    private String pass;

    public User toDomain() {
        UserCode userCode = new UserCode(this.id);
        UserMail userMail = new UserMail(this.mail);
        UserName userName = new UserName(this.name);
        UserPass userPass = new UserPass(this.pass);

        User user = new User(userName, userCode, userMail,userPass);
        return user;
    }

    public static UserDAO fromDomain(User user) {
        UserDAO userDAO = new UserDAO();
        userDAO.setMail(user.getMail().getValue());
        userDAO.setName(user.getName().getValue());
        userDAO.setPass(user.getPass().getValue());
        Long id = user.getId() == null ? 0l : user.getId().getValue();
//        Long id = null;
//        if(product.getId() == null) {
//            id = 0l;
//        } else {
//            id = product.getId().getValue();
//        }
        userDAO.setId(id);
        return userDAO;
    }
}
