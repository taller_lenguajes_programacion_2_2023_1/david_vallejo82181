package com.co.jic.template.market.respository;

import com.co.jic.template.market.DAO.BookDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<BookDAO, Long>{
}
