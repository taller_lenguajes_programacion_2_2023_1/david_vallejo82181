package com.co.jic.template.market.DAO;

import com.co.jic.template.market.domain.book.Book;
import com.co.jic.template.market.domain.book.BookCode;
import com.co.jic.template.market.domain.book.BookTitle;
import com.co.jic.template.market.domain.book.BookAuth;
import com.co.jic.template.market.domain.book.BookCopies;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "books")
@Data
public class BookDAO {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String title;

    @Column
    private String auth;

    @Column
    private Integer copies;

    public Book toDomain() {
        BookCode bookCode = new BookCode(this.id);
        BookAuth bookAuthor = new BookAuth(this.auth);
        BookTitle bookName = new BookTitle(this.title);
        BookCopies bookCopies = new BookCopies(this.copies);

        Book book = new Book(bookCode, bookName, bookAuthor, bookCopies);
        return book;
    }

    public static BookDAO fromDomain(Book book) {
        BookDAO bookDAO = new BookDAO();
        bookDAO.setAuth(book.getAuthor().getValue());
        bookDAO.setTitle(book.getName().getValue());
        bookDAO.setCopies(book.getCopies().getValue());
        Long id = book.getId() == null ? 0l : book.getId().getValue();
//        Long id = null;
//        if(product.getId() == null) {
//            id = 0l;
//        } else {
//            id = product.getId().getValue();
//        }
        bookDAO.setId(id);
        return bookDAO;
    }
}
