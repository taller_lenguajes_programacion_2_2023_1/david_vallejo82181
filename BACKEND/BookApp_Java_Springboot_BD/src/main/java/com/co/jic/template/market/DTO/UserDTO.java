package com.co.jic.template.market.DTO;

import com.co.jic.template.market.domain.user.*;
import lombok.Data;

@Data
public class UserDTO {
    private Long id;
    private String name;
    private String mail;
    private String pass;

    public User toDomain() {
        UserCode userCode = new UserCode(id);
        UserName userName = new UserName(name);
        UserMail userMail = new UserMail(mail);
        UserPass userPass = new UserPass(pass);
        User user = new User(userName, userCode, userMail, userPass);
        return user;
    }

    public static UserDTO fromDomain(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId().getValue());
        userDTO.setName(user.getName().getValue());
        userDTO.setMail(user.getMail().getValue());
        userDTO.setPass(user.getPass().getValue());
        return userDTO;
    }
}