package com.co.jic.template.market.service;

import com.co.jic.template.market.DAO.UserDAO;
import com.co.jic.template.market.DTO.UserDTO;
import com.co.jic.template.market.domain.user.*;
import com.co.jic.template.market.respository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;


    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDTO save(UserDTO userDTO){
        UserDAO userDb = this.userRepository.findByName(userDTO.getName());

        if(userDb != null) {
            throw new IllegalArgumentException("name of product not is unique");
        }

        UserName userName = new UserName(userDTO.getName());
        UserMail userMail = new UserMail(userDTO.getMail());
        UserPass userPass = new UserPass(userDTO.getPass());
        User user = new User(userName, null, userMail,userPass);

        UserDAO userForSave = UserDAO.fromDomain(user);
        UserDAO userSaved = this.userRepository.save(userForSave);
        User userSavedWithDomain = userSaved.toDomain();

        return UserDTO.fromDomain(userSavedWithDomain);
    }

    public List<UserDTO> getAllUsers() {
        List<UserDAO> userDAOS = this.userRepository.findAll();
        List<User> users = userDAOS.stream().map(userDAO -> userDAO.toDomain()).toList();
        List<UserDTO> userDTOS = users.stream().map(user -> UserDTO.fromDomain(user)).toList();
        return userDTOS;
    }

    public UserDTO updateUser(UserDTO userDTO) {
        User user = userDTO.toDomain();
        UserDAO userDAO = this.userRepository.save(UserDAO.fromDomain(user));
        return userDTO;
    }

    public void deleteUser(String id) {
        UserCode userCode = new UserCode(Long.parseLong(id));
        Optional<UserDAO> userDAO = this.userRepository.findById(userCode.getValue());
        if (userDAO.isPresent()) {
            this.userRepository.delete(userDAO.get());
        } else {
            throw new IllegalArgumentException("Product id does not exist");
        }
    }
}