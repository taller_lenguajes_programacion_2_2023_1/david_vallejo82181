package com.co.jic.template.market.domain.user;

import lombok.AllArgsConstructor;
import lombok.Data;

//value object ***
@Data
@AllArgsConstructor
public class User {
    private UserName name;
    private UserCode id;
    private UserMail mail;
    private UserPass pass;
}
