package com.co.jic.template.market.domain.book;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Book {
    private BookCode id;
    private BookTitle name;
    private BookAuth author;
    private BookCopies copies;
}
