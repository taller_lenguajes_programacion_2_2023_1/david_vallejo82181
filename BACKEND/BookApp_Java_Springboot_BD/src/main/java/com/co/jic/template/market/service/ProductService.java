package com.co.jic.template.market.service;

import com.co.jic.template.market.DAO.ProductDAO;
import com.co.jic.template.market.DTO.ProductDTO;
import com.co.jic.template.market.domain.product.Product;
import com.co.jic.template.market.domain.product.ProductCode;
import com.co.jic.template.market.domain.product.ProductName;
import com.co.jic.template.market.domain.product.ProductPrice;
import com.co.jic.template.market.respository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public ProductDTO save(ProductDTO productDTO){
        ProductDAO productDb = this.productRepository.findByName(productDTO.getName());

        if(productDb != null) {
            throw new IllegalArgumentException("name of product not is unique");
        }

        ProductName productName = new ProductName(productDTO.getName());
        ProductPrice productPrice = new ProductPrice(productDTO.getPrice());
        Product product = new Product(productName, null, productPrice);

        ProductDAO productForSave = ProductDAO.fromDomain(product);
        ProductDAO productSaved = this.productRepository.save(productForSave);
        Product productSavedWithDomain = productSaved.toDomain();

        return ProductDTO.fromDomain(productSavedWithDomain);
    }

    public List<ProductDTO> getAllProducts() {
        List<ProductDAO> productDAOS = this.productRepository.findAll();
        List<Product> products = productDAOS.stream().map(productDAO -> productDAO.toDomain()).toList();
        List<ProductDTO> productDTOS = products.stream().map(product -> ProductDTO.fromDomain(product)).toList();
        return productDTOS;
    }

    public ProductDTO updateProduct(ProductDTO productDTO) {
        Product product = productDTO.toDomain();
        ProductDAO productDAO = this.productRepository.save(ProductDAO.fromDomain(product));
        return productDTO;
    }

    public void deleteProduct(String id) {
        ProductCode productCode = new ProductCode(Long.parseLong(id));
        Optional<ProductDAO> productDAO = this.productRepository.findById(productCode.getValue());
        if (productDAO.isPresent()) {
            this.productRepository.delete(productDAO.get());
        } else {
            throw new IllegalArgumentException("Product id does not exist");
        }
    }

}
