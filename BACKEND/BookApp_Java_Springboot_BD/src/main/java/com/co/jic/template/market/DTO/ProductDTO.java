package com.co.jic.template.market.DTO;

import com.co.jic.template.market.domain.product.Product;
import com.co.jic.template.market.domain.product.ProductCode;
import com.co.jic.template.market.domain.product.ProductName;
import com.co.jic.template.market.domain.product.ProductPrice;
import lombok.Data;

@Data
public class ProductDTO {
    private Long id;
    private String name;
    private Double price;

    public Product toDomain() {
        ProductCode productCode = new ProductCode(id);
        ProductName productName = new ProductName(name);
        ProductPrice productPrice = new ProductPrice(price);
        Product product = new Product(productCode, productName, productPrice);
        return product;
    }

    public static ProductDTO fromDomain(Product product) {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(product.getId().getValue());
        productDTO.setName(product.getName().getValue());
        productDTO.setPrice(product.getPrice().getValue());
        return productDTO;
    }
}
