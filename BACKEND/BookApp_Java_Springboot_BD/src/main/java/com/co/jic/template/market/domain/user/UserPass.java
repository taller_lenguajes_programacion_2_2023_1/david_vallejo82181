package com.co.jic.template.market.domain.user;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class UserPass {
    private final String value;

    public UserPass(String value) {
        Validate.notNull(value,"la contraseña no puede ser nula");
        Validate.isTrue(value.length() <= 30, "El nombre del producto no puede ser mayor a 30 caracteres");
        this.value = value;
    }
}
