package com.co.jic.template.market.domain.product;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class ProductPrice {
    private final Double value;

    public ProductPrice(Double value) {
        Validate.notNull(value, "El precio del prodcuto no puede ser nulo");
        Validate.exclusiveBetween(1L, 1000000L, value, "El precio del producto debe de estar entre 1 y 1'");
        this.value = value;
    }
}
