package com.co.jic.template.market.DAO;

import com.co.jic.template.market.domain.product.Product;
import com.co.jic.template.market.domain.product.ProductCode;
import com.co.jic.template.market.domain.product.ProductName;
import com.co.jic.template.market.domain.product.ProductPrice;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "products")
@Data
public class ProductDAO {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private Double price;

    public Product toDomain() {
        ProductCode productCode = new ProductCode(this.id);
        ProductPrice productPrice = new ProductPrice(this.price);
        ProductName productName = new ProductName(this.name);

        Product product = new Product(productCode, productName, productPrice);
        return product;
    }

    public static ProductDAO fromDomain(Product product) {
        ProductDAO productDAO = new ProductDAO();
        productDAO.setPrice(product.getPrice().getValue());
        productDAO.setName(product.getName().getValue());
        Long id = product.getId() == null ? 0l : product.getId().getValue();
//        Long id = null;
//        if(product.getId() == null) {
//            id = 0l;
//        } else {
//            id = product.getId().getValue();
//        }
        productDAO.setId(id);
        return productDAO;
    }
}
