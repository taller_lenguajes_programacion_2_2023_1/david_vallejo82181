package com.co.jic.template.market.controller;

import com.co.jic.template.market.DTO.ProductDTO;
import com.co.jic.template.market.DTO.UserDTO;
import com.co.jic.template.market.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:4200")
@RestController
public class UserController {
    private final UserService userService;

    public UserController(UserService userService){this.userService=userService;}

    @RequestMapping(value="/users", method = RequestMethod.POST)
    public ResponseEntity<?> saveProduct(@RequestBody UserDTO userDTO) {
        try {
            UserDTO userResponse = this.userService.save(userDTO);
            return ResponseEntity.ok(userResponse);
        }  catch (NullPointerException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }

    @RequestMapping(value="/users", method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        try {
            List<UserDTO> userResponse = this.userService.getAllUsers();
            return ResponseEntity.ok(userResponse);
        }  catch (NullPointerException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }

    @RequestMapping(value="/users", method = RequestMethod.PUT)
    public ResponseEntity<?> updateUser(@RequestBody UserDTO userDTO) {
        try {
            UserDTO productResult = this.userService.updateUser(userDTO);
            return ResponseEntity.ok(productResult);
        }  catch (NullPointerException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }

    @RequestMapping(value="/users", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteUser(@RequestParam String idProduct, @RequestParam String secondParam) {
        try {
            this.userService.deleteUser(idProduct);
            return ResponseEntity.ok("Product was deleted");
        }  catch (NullPointerException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Try later");
        }
    }
}
