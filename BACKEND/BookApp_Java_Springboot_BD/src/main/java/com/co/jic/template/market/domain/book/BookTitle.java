package com.co.jic.template.market.domain.book;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class BookTitle {
    private final String value;

    public BookTitle(String value) {
        Validate.notNull(value,"El titulo del libro no puede ser nulo");
        Validate.isTrue(value.length() <= 100, "El titulo no puede ser mayor a 100 caracteres");
        this.value = value;
    }
}
