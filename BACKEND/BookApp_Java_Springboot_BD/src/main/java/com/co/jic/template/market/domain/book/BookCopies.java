package com.co.jic.template.market.domain.book;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class BookCopies {
    private final Integer value;

    public BookCopies(Integer value) {
        Validate.notNull(value,"cantidad de copias no puede ser nulo");
        Validate.isTrue(value >= 0);
        this.value = value;
    }
}
