package com.co.jic.template.market.domain.book;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class BookAuth {
    private final String value;

    public BookAuth(String value) {
        Validate.notNull(value,"El autor del libro no puede ser nulo");
        Validate.isTrue(value.length() <= 100, "El autor no puede ser mayor a 100 caracteres");
        this.value = value;
    }
}
