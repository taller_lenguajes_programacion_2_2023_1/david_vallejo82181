package com.co.jic.template.market.domain.product;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Product {
    private ProductCode id;
    private ProductName name;
    private ProductPrice price;
}
