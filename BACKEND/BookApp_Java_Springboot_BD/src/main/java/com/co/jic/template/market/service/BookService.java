package com.co.jic.template.market.service;

import com.co.jic.template.market.DAO.BookDAO;
import com.co.jic.template.market.DTO.BookDTO;
import com.co.jic.template.market.domain.book.Book;
import com.co.jic.template.market.domain.book.BookCode;
import com.co.jic.template.market.domain.book.BookTitle;
import com.co.jic.template.market.domain.book.BookAuth;
import com.co.jic.template.market.domain.book.BookCopies;
import com.co.jic.template.market.respository.BookRepository;
import com.co.jic.template.market.respository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {
    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository){this.bookRepository=bookRepository;}
}
