package com.co.jic.template.market.domain.book;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class BookCode {
    private final Long value;


    public BookCode(Long value) {
        Validate.notNull(value, "El codigo del libro no puede ser nulo");
        Validate.isTrue(value != 0);
        this.value = value;
    }
}
