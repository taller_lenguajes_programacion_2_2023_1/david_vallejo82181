package com.co.jic.template.market.respository;

import com.co.jic.template.market.DAO.UserDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserDAO, Long> {
    UserDAO findByName(String name);
    //UserDAO findByPrice(Double price);
}

