package com.co.jic.template.market.DTO;

import com.co.jic.template.market.domain.book.Book;
import com.co.jic.template.market.domain.book.BookCode;
import com.co.jic.template.market.domain.book.BookTitle;
import com.co.jic.template.market.domain.book.BookAuth;
import com.co.jic.template.market.domain.book.BookCopies;
import lombok.Data;

@Data
public class BookDTO {
    private Long id;
    private String title;
    private String auth;
    private Integer copies;

    public Book toDomain() {
        BookCode bookCode = new BookCode(id);
        BookTitle bookName = new BookTitle(title);
        BookAuth bookAuth = new BookAuth(auth);
        BookCopies bookCopies = new BookCopies(copies);
        Book book = new Book(bookCode, bookName, bookAuth, bookCopies);
        return book;
    }

    public static BookDTO fromDomain(Book book) {
        BookDTO bookDTO = new BookDTO();
        bookDTO.setId(book.getId().getValue());
        bookDTO.setTitle(book.getName().getValue());
        bookDTO.setAuth(book.getAuthor().getValue());
        bookDTO.setCopies(book.getCopies().getValue());
        return bookDTO;
    }
}
