package com.co.jic.template.market.domain.user;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

@Getter
@ToString
public class UserName {
    private final String value;

    public UserName(String value) {
        Validate.notNull(value,"El nombre del producto no puedes ser nulo");
        Validate.isTrue(value.length() <= 30, "El nombre del producto no puede ser mayor a 30 caracteres");
        this.value = value;
    }
}
