jQuery("#simulation")
  .on("click", ".s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .click", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Paragraph_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_1 span" ],
                    "attributes": {
                      "color": "#F3F3F3"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimNot",
                "parameter": [ {
                  "action": "jimRegExp",
                  "parameter": [ {
                    "datatype": "property",
                    "target": "#s-Input_3",
                    "property": "jimGetValue"
                  },"^[a-zA-Z][a-zA-Z\\s]*$" ]
                } ]
              },
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_3 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#DC3545",
                      "border-right-color": "#DC3545",
                      "border-bottom-color": "#DC3545",
                      "border-left-color": "#DC3545"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Paragraph_10" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimNot",
                "parameter": [ {
                  "action": "jimRegExp",
                  "parameter": [ {
                    "datatype": "property",
                    "target": "#s-Input_2",
                    "property": "jimGetValue"
                  },"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" ]
                } ]
              },
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_2 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#DC3545",
                      "border-right-color": "#DC3545",
                      "border-bottom-color": "#DC3545",
                      "border-left-color": "#DC3545"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Paragraph_8" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_1",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_1 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#DC3545",
                      "border-right-color": "#DC3545",
                      "border-bottom-color": "#DC3545",
                      "border-left-color": "#DC3545"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Paragraph_6" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Category_1",
                  "property": "jimGetSelectedValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options.selected" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .selected > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options:hover" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options:hover > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Category_1 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#DC3545",
                      "border-right-color": "#DC3545",
                      "border-bottom-color": "#DC3545",
                      "border-left-color": "#DC3545"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Paragraph_4" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_1" ],
                    "attributes": {
                      "filter": " drop-shadow(-1.8369701987210297E-16px 3.0px 5px #D9D9D9)"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_1 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#069BDE"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_1 > .backgroundLayer > .imageLayer" ],
                    "attributes": {
                      "background-attachment": "scroll",
                      "background-image": "none"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_1 span" ],
                    "attributes": {
                      "color": "#FFFFFF"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimAnd",
                "parameter": [ {
                  "action": "jimAnd",
                  "parameter": [ {
                    "action": "jimAnd",
                    "parameter": [ {
                      "action": "jimNot",
                      "parameter": [ {
                        "datatype": "property",
                        "target": "#s-Paragraph_10",
                        "property": "jimIsVisible"
                      } ]
                    },{
                      "action": "jimNot",
                      "parameter": [ {
                        "datatype": "property",
                        "target": "#s-Paragraph_6",
                        "property": "jimIsVisible"
                      } ]
                    } ]
                  },{
                    "action": "jimNot",
                    "parameter": [ {
                      "datatype": "property",
                      "target": "#s-Paragraph_8",
                      "property": "jimIsVisible"
                    } ]
                  } ]
                },{
                  "action": "jimNot",
                  "parameter": [ {
                    "datatype": "property",
                    "target": "#s-Paragraph_4",
                    "property": "jimIsVisible"
                  } ]
                } ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Group_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 900
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_14")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_14 span" ],
                    "attributes": {
                      "color": "#F3F3F3"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Input_3","#s-Input_1","#s-Input_2" ],
                    "value": ""
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Category_1" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Category_1",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ "#s-Paragraph_11","#s-Paragraph_5","#s-Paragraph_9","#s-Paragraph_7" ],
                    "top": {
                      "type": "movebyoffset",
                      "value": "20"
                    },
                    "left": {
                      "type": "nomove"
                    },
                    "containment": false
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Rectangle_6","#s-Rectangle_4","#s-Rectangle_5","#s-Paragraph_2","#s-Rectangle_3" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#s-Rectangle_1" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "1"
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_14" ],
                    "attributes": {
                      "filter": " drop-shadow(-1.8369701987210297E-16px 3.0px 5px #D9D9D9)"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_14 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#069BDE"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_14 > .backgroundLayer > .imageLayer" ],
                    "attributes": {
                      "background-attachment": "scroll",
                      "background-image": "none"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_14 span" ],
                    "attributes": {
                      "color": "#FFFFFF"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Group_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_3")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/3464bee7-c7b3-4871-8386-929fea17efa2"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Cell_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/3464bee7-c7b3-4871-8386-929fea17efa2"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_15")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/3464bee7-c7b3-4871-8386-929fea17efa2"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("click", ".s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .toggle", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Rectangle_2")) {
      if(jFirer.data("jimHasToggle")) {
        jFirer.removeData("jimHasToggle");
        jEvent.undoCases(jFirer);
      } else {
        jFirer.data("jimHasToggle", true);
        event.backupState = true;
        event.target = jFirer;
        cases = [
          {
            "blocks": [
              {
                "actions": [
                  {
                    "action": "jimHide",
                    "parameter": {
                      "target": [ "#s-Paragraph_2" ]
                    },
                    "exectype": "serial",
                    "delay": 0
                  },
                  {
                    "action": "jimShow",
                    "parameter": {
                      "target": [ "#s-Image_2" ]
                    },
                    "exectype": "parallel",
                    "delay": 0
                  },
                  {
                    "action": "jimResize",
                    "parameter": {
                      "target": [ "#s-Rectangle_1" ],
                      "width": {
                        "type": "noresize"
                      },
                      "height": {
                        "type": "exprvalue",
                        "value": "750"
                      },
                      "effect": {
                        "type": "none",
                        "easing": "swing",
                        "duration": 750
                      }
                    },
                    "exectype": "parallel",
                    "delay": 0
                  }
                ]
              }
            ],
            "exectype": "serial",
            "delay": 0
          }
        ];
        jEvent.launchCases(cases);
      }
    }
  })
  .on("keyup.jim", ".s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .keyup", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Input_1")) {
      cases = [
        {
          "blocks": [
            {
              "condition": {
                "action": "jimRegExp",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_1",
                  "property": "jimGetValue"
                },"^.{3,99}$" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Rectangle_4" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      if(!jimUtil.isAndroidDevice() || data.which != 229)
      	jEvent.launchCases(cases);
      if(data.which === 9) {
        return false;
      }
    } else if(jFirer.is("#s-Input_2")) {
      cases = [
        {
          "blocks": [
            {
              "condition": {
                "action": "jimRegExp",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_2",
                  "property": "jimGetValue"
                },"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Rectangle_5" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      if(!jimUtil.isAndroidDevice() || data.which != 229)
      	jEvent.launchCases(cases);
      if(data.which === 9) {
        return false;
      }
    } else if(jFirer.is("#s-Input_3")) {
      cases = [
        {
          "blocks": [
            {
              "condition": {
                "action": "jimAnd",
                "parameter": [ {
                  "action": "jimRegExp",
                  "parameter": [ {
                    "datatype": "property",
                    "target": "#s-Input_3",
                    "property": "jimGetValue"
                  },"^[a-zA-Z]+$" ]
                },{
                  "action": "jimRegExp",
                  "parameter": [ {
                    "datatype": "property",
                    "target": "#s-Input_3",
                    "property": "jimGetValue"
                  },"^.{3,99}$" ]
                } ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Rectangle_6" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      if(!jimUtil.isAndroidDevice() || data.which != 229)
      	jEvent.launchCases(cases);
      if(data.which === 9) {
        return false;
      }
    }
  })
  .on("change", ".s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .change", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Category_1")) {
      cases = [
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Category_1",
                  "property": "jimGetSelectedValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options.selected" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .selected > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options:hover" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options:hover > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Category_1 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#CED4DA",
                      "border-right-color": "#CED4DA",
                      "border-bottom-color": "#CED4DA",
                      "border-left-color": "#CED4DA"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_5 span" ],
                    "attributes": {
                      "color": "#4F4F4F"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ "#s-Paragraph_5" ],
                    "top": {
                      "type": "movetoposition",
                      "value": "493"
                    },
                    "left": {
                      "type": "nomove"
                    },
                    "containment": false,
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 270
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Rectangle_3" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#s-Rectangle_1" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "375"
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options.selected" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .selected > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options:hover" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options:hover > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Category_1 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#CED4DA",
                      "border-right-color": "#CED4DA",
                      "border-bottom-color": "#CED4DA",
                      "border-left-color": "#CED4DA"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_5 span" ],
                    "attributes": {
                      "color": "#4F4F4F"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ "#s-Paragraph_5" ],
                    "top": {
                      "type": "movetoposition",
                      "value": "473"
                    },
                    "left": {
                      "type": "nomove"
                    },
                    "containment": false,
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 270
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Paragraph_4","#s-Rectangle_3" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#s-Rectangle_1" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "500"
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("focusin", ".s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .focusin", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Category_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options.selected" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .selected > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options:hover" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options:hover > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Category_1 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#069BDE",
                      "border-right-color": "#069BDE",
                      "border-bottom-color": "#069BDE",
                      "border-left-color": "#069BDE"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_5 span" ],
                    "attributes": {
                      "color": "#069BDE"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ "#s-Paragraph_5" ],
                    "top": {
                      "type": "movetoposition",
                      "value": "473"
                    },
                    "left": {
                      "type": "nomove"
                    },
                    "containment": false,
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 270
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Paragraph_4" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Input_1")) {
      cases = [
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_1",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_1 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#069BDE",
                      "border-right-color": "#069BDE",
                      "border-bottom-color": "#069BDE",
                      "border-left-color": "#069BDE"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_7 span" ],
                    "attributes": {
                      "color": "#069BDE"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ "#s-Paragraph_7" ],
                    "top": {
                      "type": "movebyoffset",
                      "value": "-20"
                    },
                    "left": {
                      "type": "nomove"
                    },
                    "containment": false,
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 270
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Paragraph_6" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_1 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#069BDE",
                      "border-right-color": "#069BDE",
                      "border-bottom-color": "#069BDE",
                      "border-left-color": "#069BDE"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_7 span" ],
                    "attributes": {
                      "color": "#069BDE"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Paragraph_6" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Input_2")) {
      cases = [
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_2",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_2 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#069BDE",
                      "border-right-color": "#069BDE",
                      "border-bottom-color": "#069BDE",
                      "border-left-color": "#069BDE"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_9 span" ],
                    "attributes": {
                      "color": "#069BDE"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ "#s-Paragraph_9" ],
                    "top": {
                      "type": "movetoposition",
                      "value": "322"
                    },
                    "left": {
                      "type": "nomove"
                    },
                    "containment": false,
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 270
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Paragraph_8" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_2 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#069BDE",
                      "border-right-color": "#069BDE",
                      "border-bottom-color": "#069BDE",
                      "border-left-color": "#069BDE"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_9 span" ],
                    "attributes": {
                      "color": "#069BDE"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Paragraph_8" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Input_3")) {
      cases = [
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_3",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_3 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#069BDE",
                      "border-right-color": "#069BDE",
                      "border-bottom-color": "#069BDE",
                      "border-left-color": "#069BDE"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_11 span" ],
                    "attributes": {
                      "color": "#069BDE"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ "#s-Paragraph_11" ],
                    "top": {
                      "type": "movebyoffset",
                      "value": "-20"
                    },
                    "left": {
                      "type": "nomove"
                    },
                    "containment": false,
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 270
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Paragraph_10" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_3 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#069BDE",
                      "border-right-color": "#069BDE",
                      "border-bottom-color": "#069BDE",
                      "border-left-color": "#069BDE"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_11 span" ],
                    "attributes": {
                      "color": "#069BDE"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Paragraph_10" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("focusout", ".s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .focusout", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Category_1")) {
      cases = [
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Category_1",
                  "property": "jimGetSelectedValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options.selected" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .selected > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options:hover" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options:hover > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Category_1 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#CED4DA",
                      "border-right-color": "#CED4DA",
                      "border-bottom-color": "#CED4DA",
                      "border-left-color": "#CED4DA"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_5 span" ],
                    "attributes": {
                      "color": "#4F4F4F"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ "#s-Paragraph_5" ],
                    "top": {
                      "type": "movetoposition",
                      "value": "493"
                    },
                    "left": {
                      "type": "nomove"
                    },
                    "containment": false,
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 270
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Rectangle_3" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#s-Rectangle_1" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "375"
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options.selected" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .selected > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options:hover" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .s-Category_1 .line_options:hover > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Category_1 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#CED4DA",
                      "border-right-color": "#CED4DA",
                      "border-bottom-color": "#CED4DA",
                      "border-left-color": "#CED4DA"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_5 span" ],
                    "attributes": {
                      "color": "#4F4F4F"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ "#s-Paragraph_5" ],
                    "top": {
                      "type": "movetoposition",
                      "value": "473"
                    },
                    "left": {
                      "type": "nomove"
                    },
                    "containment": false,
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 270
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Paragraph_4","#s-Rectangle_3" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#s-Rectangle_1" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "500"
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Input_1")) {
      cases = [
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_1",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_1 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#CED4DA",
                      "border-right-color": "#CED4DA",
                      "border-bottom-color": "#CED4DA",
                      "border-left-color": "#CED4DA"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_7 span" ],
                    "attributes": {
                      "color": "#4F4F4F"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Paragraph_6" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ "#s-Paragraph_7" ],
                    "top": {
                      "type": "movebyoffset",
                      "value": "20"
                    },
                    "left": {
                      "type": "nomove"
                    },
                    "containment": false,
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 270
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_1 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#CED4DA",
                      "border-right-color": "#CED4DA",
                      "border-bottom-color": "#CED4DA",
                      "border-left-color": "#CED4DA"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_7 span" ],
                    "attributes": {
                      "color": "#4F4F4F"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Paragraph_6" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#s-Rectangle_1" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "375"
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Input_2")) {
      cases = [
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_2",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_2 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#DC3545",
                      "border-right-color": "#DC3545",
                      "border-bottom-color": "#DC3545",
                      "border-left-color": "#DC3545"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_9 span" ],
                    "attributes": {
                      "color": "#DC3545"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ "#s-Paragraph_9" ],
                    "top": {
                      "type": "movetoposition",
                      "value": "342"
                    },
                    "left": {
                      "type": "nomove"
                    },
                    "containment": false,
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 270
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Paragraph_8" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimNot",
                "parameter": [ {
                  "action": "jimRegExp",
                  "parameter": [ {
                    "datatype": "property",
                    "target": "#s-Input_2",
                    "property": "jimGetValue"
                  },"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" ]
                } ]
              },
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_2 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#DC3545",
                      "border-right-color": "#DC3545",
                      "border-bottom-color": "#DC3545",
                      "border-left-color": "#DC3545"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_9 span" ],
                    "attributes": {
                      "color": "#DC3545"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Paragraph_8" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_2 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#CED4DA",
                      "border-right-color": "#CED4DA",
                      "border-bottom-color": "#CED4DA",
                      "border-left-color": "#CED4DA"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_9 span" ],
                    "attributes": {
                      "color": "#4F4F4F"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Paragraph_8" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#s-Rectangle_1" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "250"
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Input_3")) {
      cases = [
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_3",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_3 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#DC3545",
                      "border-right-color": "#DC3545",
                      "border-bottom-color": "#DC3545",
                      "border-left-color": "#DC3545"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_11 span" ],
                    "attributes": {
                      "color": "#DC3545"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Paragraph_10" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ "#s-Paragraph_11" ],
                    "top": {
                      "type": "movebyoffset",
                      "value": "20"
                    },
                    "left": {
                      "type": "nomove"
                    },
                    "containment": false,
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 270
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimNot",
                "parameter": [ {
                  "action": "jimRegExp",
                  "parameter": [ {
                    "datatype": "property",
                    "target": "#s-Input_3",
                    "property": "jimGetValue"
                  },"^[a-zA-Z][a-zA-Z\\s]*$" ]
                } ]
              },
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_3 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#DC3545",
                      "border-right-color": "#DC3545",
                      "border-bottom-color": "#DC3545",
                      "border-left-color": "#DC3545"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_11 span" ],
                    "attributes": {
                      "color": "#DC3545"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Paragraph_10" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Input_3 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#CED4DA",
                      "border-right-color": "#CED4DA",
                      "border-bottom-color": "#CED4DA",
                      "border-left-color": "#CED4DA"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_11 span" ],
                    "attributes": {
                      "color": "#777777"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Paragraph_10" ]
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#s-Rectangle_1" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "125"
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("mouseenter dragenter", ".s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .mouseenter", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getDirectEventFirer(this);
    if(jFirer.is("#s-Paragraph_1") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_1" ],
                    "attributes": {
                      "opacity": "0.75"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_14") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_14" ],
                    "attributes": {
                      "opacity": "0.75"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_15") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 #s-Paragraph_15 span" ],
                    "attributes": {
                      "color": "#000000"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeCursor",
                  "parameter": {
                    "type": "pointer"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    }
  })
  .on("mouseleave dragleave", ".s-6b1dd84d-c963-4cc3-9044-f468318fcdc2 .mouseleave", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getDirectEventFirer(this);
    if(jFirer.is("#s-Paragraph_1")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Paragraph_14")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Paragraph_15")) {
      jEvent.undoCases(jFirer);
    }
  });