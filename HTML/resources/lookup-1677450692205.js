(function(window, undefined) {
  var dictionary = {
    "6b1dd84d-c963-4cc3-9044-f468318fcdc2": "register",
    "3464bee7-c7b3-4871-8386-929fea17efa2": "Landing",
    "a5346870-5110-4db9-b8b8-81b5061532d2": "Shop",
    "4a8719bc-45cd-4fcd-9f5a-176c030e8a78": "login",
    "d7263a04-576f-4eb3-92eb-8583780e1c18": "Estadisticas",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);