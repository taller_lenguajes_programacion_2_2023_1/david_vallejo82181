var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1440" deviceHeight="768">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1440" height="768">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677450692205.css" />\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-d7263a04-576f-4eb3-92eb-8583780e1c18" class="screen growth-vertical devWeb canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="Estadisticas" width="1440" height="768">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/d7263a04-576f-4eb3-92eb-8583780e1c18-1677450692205.css" />\
      <div class="freeLayout">\
      <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Repayments" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Path_1" class="path firer commentable non-processed" customid="BG"   datasizewidth="456.2px" datasizeheight="375.5px" dataX="46.0" dataY="504.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="456.181396484375" height="375.4722900390625" viewBox="46.049968539230804 504.0 456.181396484375 375.4722900390625" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_1-d7263" d="M47.049968539230804 525.0 A20.0 20.0 0.0 0 1 67.0499685392308 505.0 L481.2313596591698 505.0 A20.0 20.0 0.0 0 1 501.2313596591698 525.0 L501.2313596591698 858.4722222222222 A20.0 20.0 0.0 0 1 481.2313596591698 878.4722222222222 L67.0499685392308 878.4722222222222 A20.0 20.0 0.0 0 1 47.049968539230804 858.4722222222222 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_1-d7263" fill="#FFFFFF" fill-opacity="1.0" stroke-width="1.0" stroke="#E7E7E7" stroke-linecap="butt"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_2" class="group firer ie-background commentable non-processed" customid="Graphic" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Path_2" class="path firer ie-background commentable non-processed" customid="Path 46"   datasizewidth="378.9px" datasizeheight="3.0px" dataX="84.9" dataY="820.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="378.938720703125" height="2.0" viewBox="84.89880449077054 819.9999945226375 378.938720703125 2.0" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_2-d7263" d="M85.89880449077054 821.0000000000002 L462.83751007463 821.0000000000002 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_2-d7263" fill="none" stroke-width="1.0" stroke="#D2D2D2" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Group_3" class="group firer ie-background commentable non-processed" customid="Weekdays" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Paragraph_1" class="richtext autofit firer ie-background commentable non-processed" customid="M"   datasizewidth="11.8px" datasizeheight="19.0px" dataX="100.6" dataY="834.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_1_0">M</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_2" class="richtext autofit firer ie-background commentable non-processed" customid="T"   datasizewidth="8.4px" datasizeheight="19.0px" dataX="163.6" dataY="834.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_2_0">T</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_3" class="richtext autofit firer ie-background commentable non-processed" customid="W"   datasizewidth="15.4px" datasizeheight="19.0px" dataX="215.0" dataY="834.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_3_0">W</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_4" class="richtext autofit firer ie-background commentable non-processed" customid="T"   datasizewidth="8.4px" datasizeheight="19.0px" dataX="275.9" dataY="834.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_4_0">T</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_5" class="richtext autofit firer ie-background commentable non-processed" customid="F"   datasizewidth="7.8px" datasizeheight="19.0px" dataX="330.5" dataY="834.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_5_0">F</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_6" class="richtext autofit firer ie-background commentable non-processed" customid="S"   datasizewidth="8.6px" datasizeheight="19.0px" dataX="387.2" dataY="834.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_6_0">S</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_7" class="richtext autofit firer ie-background commentable non-processed" customid="S"   datasizewidth="8.6px" datasizeheight="19.0px" dataX="445.0" dataY="834.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_7_0">S</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
\
          <div id="s-Group_4" class="group firer ie-background commentable non-processed" customid="Bars" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Path_3" class="path firer ie-background commentable non-processed" customid="Path"   datasizewidth="3.0px" datasizeheight="91.0px" dataX="103.8" dataY="707.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="91.0" viewBox="103.79823819692524 706.9999945226373 2.0 91.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_3-d7263" d="M104.79823819692524 797.0 L104.79823819692524 708.0 "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_3-d7263" fill="none" stroke-width="1.0" stroke="#D2D2D2" stroke-linecap="square"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_4" class="path firer ie-background commentable non-processed" customid="Path"   datasizewidth="3.0px" datasizeheight="91.0px" dataX="160.5" dataY="707.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="91.0" viewBox="160.49653931538887 706.9999945226373 2.0 91.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_4-d7263" d="M161.49653931538887 797.0 L161.49653931538887 708.0 "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_4-d7263" fill="none" stroke-width="1.0" stroke="#D2D2D2" stroke-linecap="square"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_5" class="path firer ie-background commentable non-processed" customid="Path"   datasizewidth="3.0px" datasizeheight="91.0px" dataX="217.2" dataY="707.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="91.0" viewBox="217.19484043385228 706.9999945226373 2.0 91.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_5-d7263" d="M218.19484043385228 797.0 L218.19484043385228 708.0 "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_5-d7263" fill="none" stroke-width="1.0" stroke="#D2D2D2" stroke-linecap="square"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_6" class="path firer ie-background commentable non-processed" customid="Path "   datasizewidth="3.0px" datasizeheight="91.0px" dataX="276.0" dataY="707.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="91.0" viewBox="275.9930786307775 706.9999945226373 2.0 91.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_6-d7263" d="M276.9930786307775 797.0 L276.9930786307775 708.0 "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_6-d7263" fill="none" stroke-width="1.0" stroke="#D2D2D2" stroke-linecap="square"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_7" class="path firer ie-background commentable non-processed" customid="Path"   datasizewidth="3.0px" datasizeheight="91.0px" dataX="332.7" dataY="707.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="91.0" viewBox="332.69137974924115 706.9999945226373 2.0 91.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_7-d7263" d="M333.69137974924115 797.0 L333.69137974924115 708.0 "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_7-d7263" fill="none" stroke-width="1.0" stroke="#D2D2D2" stroke-linecap="square"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_8" class="path firer ie-background commentable non-processed" customid="Path"   datasizewidth="3.0px" datasizeheight="91.0px" dataX="388.3" dataY="707.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="91.0" viewBox="388.33971232847375 706.9999945226373 2.0 91.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_8-d7263" d="M389.33971232847375 797.0 L389.33971232847375 708.0 "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_8-d7263" fill="none" stroke-width="1.0" stroke="#D2D2D2" stroke-linecap="square"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_9" class="path firer ie-background commentable non-processed" customid="Path "   datasizewidth="3.0px" datasizeheight="91.0px" dataX="447.1" dataY="707.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="91.0" viewBox="447.137950525399 706.9999945226373 2.0 91.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_9-d7263" d="M448.137950525399 797.0 L448.137950525399 708.0 "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_9-d7263" fill="none" stroke-width="1.0" stroke="#D2D2D2" stroke-linecap="square"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Path_10" class="path firer ie-background commentable non-processed" customid="Path "   datasizewidth="366.9px" datasizeheight="57.7px" dataX="91.7" dataY="726.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="360.9947509765625" height="54.26213073730469" viewBox="91.6986157261556 725.9999980584782 360.9947509765625 54.26213073730469" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_10-d7263" d="M92.1986157261556 770.5708675042789 C92.1986157261556 770.5708675042789 159.22107879532408 737.8186016432519 168.28908503141992 737.8186016432519 C177.35709126751578 737.8186016432519 200.59385724751237 748.614170626132 206.54473633995025 747.534613727844 C212.4956154323881 746.4550568295559 239.69963414067635 737.8186016432521 247.6341395972604 737.818601643252 C255.56864505384445 737.8186016432519 284.18953973652265 768.7773126200899 291.8406699982288 770.5708675042791 C299.49180025993496 772.3644223884683 332.36332286578295 772.0159833149544 338.03082676334316 770.5708675042794 C343.69833066090337 769.1257516936042 378.55347963089775 731.3412602535237 386.7713602823597 729.1821464569477 C394.98924093382163 727.0230326603718 415.95900535479393 744.7007768698381 423.0433852267438 748.2768090954171 C430.1277650986937 751.852841320996 437.495520165522 773.4439792867561 449.11390315552006 776.6826499816202 "></path>\
              	      <filter filterUnits="userSpaceOnUse" x="53.698615726155595" y="687.9999980584782" width="436.9947509765625" height="162.2621307373047" color-interpolation-filters="sRGB" id="s-Path_10-d7263_effects">\
              	        <feOffset dx="-1.959434878635765E-15" dy="32.0" input="SourceAlpha"></feOffset>\
              	        <feGaussianBlur result="DROP_SHADOW_0_blur" stdDeviation="38"></feGaussianBlur>\
              	        <feFlood flood-color="#D1D1D1" flood-opacity="0.2"></feFlood>\
              	        <feComposite operator="in" in2="DROP_SHADOW_0_blur"></feComposite>\
              	        <feComposite in="SourceGraphic" result="DROP_SHADOW_0"></feComposite>\
              	      </filter>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal" filter="url(#s-Path_10-d7263_effects)">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_10-d7263" fill="none" stroke-width="5.0" stroke="#35B736" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Button_1" class="button multiline manualfit firer commentable non-processed" customid="Button - See more"   datasizewidth="155.4px" datasizeheight="47.0px" dataX="88.0" dataY="608.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Button_1_0">Reserva</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_8" class="richtext autofit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="149.1px" datasizeheight="40.0px" dataX="324.0" dataY="611.5" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_8_0">12/semana</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_5" class="group firer ie-background commentable non-processed" customid="Header" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Group_6" class="group firer ie-background commentable non-processed" customid="Dropdown" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Path_11" class="path firer commentable non-processed" customid="arrow icon"   datasizewidth="12.6px" datasizeheight="6.4px" dataX="163.6" dataY="574.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="12.599621772766113" height="6.421226501464844" viewBox="163.5964766024665 573.9999992549419 12.599621772766113 6.421226501464844" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_11-d7263" d="M176.06718188094985 574.1240816832609 C175.89352540338402 573.958639438913 175.6113336548337 573.958639438913 175.4376771384526 574.1240816832609 L169.90237699852443 579.407893125607 L164.35622349452186 574.1240816832609 C164.18256701695603 573.958639438913 163.9003752684057 573.958639438913 163.72671875202462 574.1240816832609 C163.5530622744588 574.2895239276087 163.5530622744588 574.5583675484803 163.72671875202462 574.7238098298074 L169.5767714184624 580.297145511004 C169.66359965724533 580.3798666331779 169.77213494763745 580.4212271973465 169.89152378971102 580.4212271973465 C170.00005908657235 580.4212271973465 170.11944790276908 580.3798666362595 170.20627616095962 580.297145511004 L176.0563288273974 574.7238098298074 C176.24083883723705 574.5583675854596 176.24083883723705 574.2895239645879 176.06718235724526 574.1240816832609 Z "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_11-d7263" fill="#000000" fill-opacity="1.0"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_9" class="richtext autofit firer ie-background commentable non-processed" customid="Last week"   datasizewidth="63.6px" datasizeheight="19.0px" dataX="88.0" dataY="567.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_9_0">Last week</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Paragraph_10" class="richtext autofit firer ie-background commentable non-processed" customid="Repayments"   datasizewidth="106.4px" datasizeheight="34.0px" dataX="88.0" dataY="535.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_10_0">Reservas</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
      </div>\
\
\
      <div id="s-Group_7" class="group firer ie-background commentable non-processed" customid="Ads Expense" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Path_12" class="path firer commentable non-processed" customid="BG"   datasizewidth="456.2px" datasizeheight="375.5px" dataX="535.3" dataY="504.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="456.181396484375" height="375.4722900390625" viewBox="535.3353078207865 504.0 456.181396484375 375.4722900390625" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_12-d7263" d="M536.3353078207865 525.0 A20.0 20.0 0.0 0 1 556.3353078207865 505.0 L970.5166989407256 505.0 A20.0 20.0 0.0 0 1 990.5166989407256 525.0 L990.5166989407256 858.4722222222222 A20.0 20.0 0.0 0 1 970.5166989407256 878.4722222222222 L556.3353078207865 878.4722222222222 A20.0 20.0 0.0 0 1 536.3353078207865 858.4722222222222 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_12-d7263" fill="#FFFFFF" fill-opacity="1.0" stroke-width="1.0" stroke="#E7E7E7" stroke-linecap="butt"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_8" class="group firer ie-background commentable non-processed" customid="Graphic" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Path_13" class="path firer ie-background commentable non-processed" customid="Path"   datasizewidth="378.9px" datasizeheight="3.0px" dataX="574.2" dataY="820.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="378.938720703125" height="2.0" viewBox="574.1841437723265 819.9999945226375 378.938720703125 2.0" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_13-d7263" d="M575.1841437723265 821.0000000000002 L952.122849356186 821.0000000000002 "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_13-d7263" fill="none" stroke-width="1.0" stroke="#D2D2D2" stroke-linecap="square"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Group_9" class="group firer ie-background commentable non-processed" customid="Weekdays" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Paragraph_11" class="richtext autofit firer ie-background commentable non-processed" customid="M"   datasizewidth="11.8px" datasizeheight="19.0px" dataX="589.9" dataY="834.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_11_0">M</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_12" class="richtext autofit firer ie-background commentable non-processed" customid="T"   datasizewidth="8.4px" datasizeheight="19.0px" dataX="652.9" dataY="834.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_12_0">T</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_13" class="richtext autofit firer ie-background commentable non-processed" customid="W"   datasizewidth="15.4px" datasizeheight="19.0px" dataX="704.3" dataY="834.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_13_0">W</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_14" class="richtext autofit firer ie-background commentable non-processed" customid="T"   datasizewidth="8.4px" datasizeheight="19.0px" dataX="765.2" dataY="834.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_14_0">T</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_15" class="richtext autofit firer ie-background commentable non-processed" customid="F"   datasizewidth="7.8px" datasizeheight="19.0px" dataX="819.8" dataY="834.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_15_0">F</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_16" class="richtext autofit firer ie-background commentable non-processed" customid="S"   datasizewidth="8.6px" datasizeheight="19.0px" dataX="876.5" dataY="834.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_16_0">S</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_17" class="richtext autofit firer ie-background commentable non-processed" customid="S"   datasizewidth="8.6px" datasizeheight="19.0px" dataX="930.1" dataY="834.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_17_0">S</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Path_14" class="path firer commentable non-processed" customid="Path 39"   datasizewidth="40.2px" datasizeheight="120.0px" dataX="690.7" dataY="692.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="40.2021484375" height="120.0" viewBox="690.6806830877156 691.9999999999998 40.2021484375 120.0" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_14-d7263" d="M690.6806830877156 701.9999999999998 A10.0 10.0 0.0 0 1 700.6806830877156 691.9999999999998 L720.8828031495009 691.9999999999998 A10.0 10.0 0.0 0 1 730.8828031495009 701.9999999999998 L730.8828031495009 801.9999999999998 A10.0 10.0 0.0 0 1 720.8828031495009 811.9999999999998 L700.6806830877156 811.9999999999998 A10.0 10.0 0.0 0 1 690.6806830877156 801.9999999999998 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_14-d7263" fill="#35B736" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_15" class="path firer commentable non-processed" customid="Path 39"   datasizewidth="40.2px" datasizeheight="60.5px" dataX="747.4" dataY="752.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="40.2021484375" height="60.465087890625" viewBox="747.3789842061788 752.0 40.2021484375 60.465087890625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_15-d7263" d="M747.3789842061788 762.0 A10.0 10.0 0.0 0 1 757.3789842061788 752.0 L777.5811042679638 752.0 A10.0 10.0 0.0 0 1 787.5811042679638 762.0 L787.5811042679638 802.4651162790697 A10.0 10.0 0.0 0 1 777.5811042679638 812.4651162790697 L757.3789842061788 812.4651162790697 A10.0 10.0 0.0 0 1 747.3789842061788 802.4651162790697 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_15-d7263" fill="#000000" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_16" class="path firer commentable non-processed" customid="Path 39"   datasizewidth="40.2px" datasizeheight="87.4px" dataX="800.9" dataY="725.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="40.2021484375" height="87.44183349609375" viewBox="800.9273797069498 725.0 40.2021484375 87.44183349609375" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_16-d7263" d="M800.9273797069498 735.0 A10.0 10.0 0.0 0 1 810.9273797069498 725.0 L831.1294997687352 725.0 A10.0 10.0 0.0 0 1 841.1294997687352 735.0 L841.1294997687352 802.4418604651164 A10.0 10.0 0.0 0 1 831.1294997687352 812.4418604651164 L810.9273797069498 812.4418604651164 A10.0 10.0 0.0 0 1 800.9273797069498 802.4418604651164 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_16-d7263" fill="#35B736" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_17" class="path firer commentable non-processed" customid="Path 39"   datasizewidth="40.2px" datasizeheight="102.3px" dataX="857.6" dataY="710.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="40.2021484375" height="102.3255615234375" viewBox="857.6256808254129 710.0 40.2021484375 102.3255615234375" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_17-d7263" d="M857.6256808254129 720.0 A10.0 10.0 0.0 0 1 867.6256808254129 710.0 L887.8278008871981 710.0 A10.0 10.0 0.0 0 1 897.8278008871981 720.0 L897.8278008871981 802.3255813953488 A10.0 10.0 0.0 0 1 887.8278008871981 812.3255813953488 L867.6256808254129 812.3255813953488 A10.0 10.0 0.0 0 1 857.6256808254129 802.3255813953488 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_17-d7263" fill="#000000" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_18" class="path firer commentable non-processed" customid="Path 39"   datasizewidth="40.2px" datasizeheight="66.0px" dataX="578.3" dataY="746.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="40.2021484375" height="66.0465087890625" viewBox="578.3340493900189 746.0 40.2021484375 66.0465087890625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_18-d7263" d="M578.3340493900189 756.0 A10.0 10.0 0.0 0 1 588.3340493900189 746.0 L608.5361694518043 746.0 A10.0 10.0 0.0 0 1 618.5361694518043 756.0 L618.5361694518043 802.046511627907 A10.0 10.0 0.0 0 1 608.5361694518043 812.046511627907 L588.3340493900189 812.046511627907 A10.0 10.0 0.0 0 1 578.3340493900189 802.046511627907 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_18-d7263" fill="#35B736" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_19" class="path firer commentable non-processed" customid="Path 39"   datasizewidth="40.2px" datasizeheight="94.9px" dataX="635.0" dataY="717.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="40.2021484375" height="94.88372802734375" viewBox="635.0323505084823 716.9999999999998 40.2021484375 94.88372802734375" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_19-d7263" d="M635.0323505084823 726.9999999999998 A10.0 10.0 0.0 0 1 645.0323505084823 716.9999999999998 L665.2344705702675 716.9999999999998 A10.0 10.0 0.0 0 1 675.2344705702675 726.9999999999998 L675.2344705702675 801.8837209302324 A10.0 10.0 0.0 0 1 665.2344705702675 811.8837209302324 L645.0323505084823 811.8837209302324 A10.0 10.0 0.0 0 1 635.0323505084823 801.8837209302324 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_19-d7263" fill="#000000" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_20" class="path firer commentable non-processed" customid="Path 39"   datasizewidth="40.2px" datasizeheight="77.2px" dataX="911.2" dataY="735.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="40.2021484375" height="77.20928955078125" viewBox="911.1740763261851 735.0 40.2021484375 77.20928955078125" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_20-d7263" d="M911.1740763261851 745.0 A10.0 10.0 0.0 0 1 921.1740763261851 735.0 L941.3761963879703 735.0 A10.0 10.0 0.0 0 1 951.3761963879703 745.0 L951.3761963879703 802.2093023255816 A10.0 10.0 0.0 0 1 941.3761963879703 812.2093023255816 L921.1740763261851 812.2093023255816 A10.0 10.0 0.0 0 1 911.1740763261851 802.2093023255816 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_20-d7263" fill="#35B736" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Paragraph_18" class="richtext autofit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="66.8px" datasizeheight="40.0px" dataX="859.0" dataY="611.5" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_18_0">7/dia</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Button_2" class="button multiline manualfit firer commentable non-processed" customid="Button - Add money"   datasizewidth="155.4px" datasizeheight="47.0px" dataX="577.3" dataY="608.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Button_2_0">Ver Prestamos</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_10" class="group firer ie-background commentable non-processed" customid="Header" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Group_11" class="group firer ie-background commentable non-processed" customid="Dropdown" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Path_21" class="path firer commentable non-processed" customid="arrow icon"   datasizewidth="12.6px" datasizeheight="6.4px" dataX="652.9" dataY="574.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="12.599621772766113" height="6.421226501464844" viewBox="652.8818158840227 573.9999992549419 12.599621772766113 6.421226501464844" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_21-d7263" d="M665.352521162506 574.1240816832609 C665.1788646849402 573.958639438913 664.8966729363898 573.958639438913 664.7230164200088 574.1240816832609 L659.1877162800806 579.407893125607 L653.641562776078 574.1240816832609 C653.4679062985122 573.958639438913 653.1857145499619 573.958639438913 653.0120580335808 574.1240816832609 C652.838401556015 574.2895239276087 652.838401556015 574.5583675484803 653.0120580335808 574.7238098298074 L658.8621107000185 580.297145511004 C658.9489389388015 580.3798666331779 659.0574742291936 580.4212271973465 659.1768630712671 580.4212271973465 C659.2853983681285 580.4212271973465 659.4047871843252 580.3798666362595 659.4916154425158 580.297145511004 L665.3416681089535 574.7238098298074 C665.5261781187932 574.5583675854596 665.5261781187932 574.2895239645879 665.3525216388014 574.1240816832609 Z "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_21-d7263" fill="#000000" fill-opacity="1.0"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Paragraph_19" class="richtext autofit firer ie-background commentable non-processed" customid="Last week"   datasizewidth="63.6px" datasizeheight="19.0px" dataX="577.3" dataY="567.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_19_0">Last week</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Paragraph_20" class="richtext manualfit firer ie-background commentable non-processed" customid="Ads Expense"   datasizewidth="139.4px" datasizeheight="116.0px" dataX="577.3" dataY="535.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_20_0">Prestamos</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
      </div>\
\
\
      <div id="s-Group_12" class="group firer ie-background commentable non-processed" customid="Cards " datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Group_13" class="group firer ie-background commentable non-processed" customid="Received packages" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Path_22" class="path firer commentable non-processed" customid="Polygon 5"   datasizewidth="298.9px" datasizeheight="106.3px" dataX="1024.0" dataY="773.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="298.915283203125" height="106.34722900390625" viewBox="1024.0 773.0 298.915283203125 106.34722900390625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_22-d7263" d="M1025.0 789.0 A15.0 15.0 0.0 0 1 1040.0 774.0 L1306.9152777777776 774.0 A15.0 15.0 0.0 0 1 1321.9152777777776 789.0 L1321.9152777777776 863.3472222222222 A15.0 15.0 0.0 0 1 1306.9152777777776 878.3472222222222 L1040.0 878.3472222222222 A15.0 15.0 0.0 0 1 1025.0 863.3472222222222 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_22-d7263" fill="#FFFFFF" fill-opacity="1.0" stroke-width="1.0" stroke="#E7E7E7" stroke-linecap="butt"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_21" class="richtext autofit firer ie-background commentable non-processed" customid="182"   datasizewidth="50.4px" datasizeheight="38.0px" dataX="1239.0" dataY="798.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_21_0">182</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_22" class="richtext autofit firer ie-background commentable non-processed" customid="Received packages"   datasizewidth="119.6px" datasizeheight="19.0px" dataX="1170.0" dataY="832.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_22_0">Received packages</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Group_14" class="group firer ie-background commentable non-processed" customid="Received packages - icon" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Group_15" class="group firer ie-background commentable non-processed" customid="Fllled" datasizewidth="0.0px" datasizeheight="0.0px" >\
              <div id="s-Path_23" class="path firer commentable non-processed" customid="Path"   datasizewidth="29.9px" datasizeheight="29.9px" dataX="1074.0" dataY="808.0"  >\
                <div class="borderLayer">\
                	<div class="imageViewport">\
                  	<?xml version="1.0" encoding="UTF-8"?>\
                  	<svg xmlns="http://www.w3.org/2000/svg" width="29.86349105834961" height="29.86349105834961" viewBox="1074.0 808.0 29.86349105834961 29.86349105834961" preserveAspectRatio="none">\
                  	  <g>\
                  	    <defs>\
                  	      <path id="s-Path_23-d7263" d="M1093.0794521189837 808.0 L1084.784038154208 808.0 L1074.0 808.0 L1074.0 837.8634902731917 L1103.8634902731917 837.8634902731917 L1103.8634902731917 808.0 L1093.0794521189837 808.0 Z "></path>\
                  	    </defs>\
                  	    <g style="mix-blend-mode:normal">\
                  	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_23-d7263" fill="#35B736" fill-opacity="1.0"></use>\
                  	    </g>\
                  	  </g>\
                  	</svg>\
\
                  </div>\
                </div>\
              </div>\
              <div id="s-Path_24" class="path firer commentable non-processed" customid="Path"   datasizewidth="44.8px" datasizeheight="9.1px" dataX="1066.0" dataY="840.0"  >\
                <div class="borderLayer">\
                	<div class="imageViewport">\
                  	<?xml version="1.0" encoding="UTF-8"?>\
                  	<svg xmlns="http://www.w3.org/2000/svg" width="44.79523468017578" height="9.124954223632812" viewBox="1066.0 840.0 44.79523468017578 9.124954223632812" preserveAspectRatio="none">\
                  	  <g>\
                  	    <defs>\
                  	      <path id="s-Path_24-d7263" d="M1100.840738652057 840.0 L1093.3748660837591 840.0 L1083.4203693260285 840.0 L1075.9544967577306 840.0 L1066.8295413964775 840.0 L1066.0 840.0 L1066.0 849.124955361253 L1075.9544967577306 849.124955361253 L1075.9544967577306 841.6590827929551 L1083.4203693260285 841.6590827929551 L1083.4203693260285 849.124955361253 L1093.3748660837591 849.124955361253 L1093.3748660837591 841.6590827929551 L1100.840738652057 841.6590827929551 L1100.840738652057 849.124955361253 L1110.7952354097874 849.124955361253 L1110.7952354097874 841.6590827929551 L1110.7952354097874 840.0 L1100.840738652057 840.0 Z "></path>\
                  	    </defs>\
                  	    <g style="mix-blend-mode:normal">\
                  	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_24-d7263" fill="#35B736" fill-opacity="1.0"></use>\
                  	    </g>\
                  	  </g>\
                  	</svg>\
\
                  </div>\
                </div>\
              </div>\
            </div>\
\
\
            <div id="s-Group_16" class="group firer ie-background commentable non-processed" customid="Stroke" datasizewidth="0.0px" datasizeheight="0.0px" >\
              <div id="s-Group_17" class="group firer ie-background commentable non-processed" customid="Group " datasizewidth="0.0px" datasizeheight="0.0px" >\
                <div id="s-Path_25" class="path firer commentable non-processed" customid="Path"   datasizewidth="29.9px" datasizeheight="29.9px" dataX="1071.0" dataY="806.0"  >\
                  <div class="borderLayer">\
                  	<div class="imageViewport">\
                    	<?xml version="1.0" encoding="UTF-8"?>\
                    	<svg xmlns="http://www.w3.org/2000/svg" width="29.86349105834961" height="29.86349105834961" viewBox="1071.0 806.0 29.86349105834961 29.86349105834961" preserveAspectRatio="none">\
                    	  <g>\
                    	    <defs>\
                    	      <path id="s-Path_25-d7263" d="M1090.0794521189837 806.0 L1081.7840381542082 806.0 L1071.0 806.0 L1071.0 835.8634902731917 L1100.863490273192 835.8634902731917 L1100.863490273192 806.0 L1090.0794521189837 806.0 Z M1088.4203693260285 807.6590827929551 L1088.4203693260285 814.2954139647754 L1083.4431209471634 814.2954139647754 L1083.4431209471634 807.6590827929551 L1088.4203693260285 807.6590827929551 Z M1099.2044074802368 834.2044074802366 L1072.6590827929551 834.2044074802366 L1072.6590827929551 807.6590827929551 L1081.7840381542082 807.6590827929551 L1081.7840381542082 815.9544967577306 L1090.0794521189837 815.9544967577306 L1090.0794521189837 807.6590827929551 L1099.2044074802368 807.6590827929551 L1099.2044074802368 834.2044074802366 Z "></path>\
                    	    </defs>\
                    	    <g style="mix-blend-mode:normal">\
                    	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_25-d7263" fill="#000000" fill-opacity="1.0"></use>\
                    	    </g>\
                    	  </g>\
                    	</svg>\
\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Path_26" class="path firer commentable non-processed" customid="Path"   datasizewidth="10.0px" datasizeheight="6.6px" dataX="1075.0" dataY="826.0"  >\
                  <div class="borderLayer">\
                  	<div class="imageViewport">\
                    	<?xml version="1.0" encoding="UTF-8"?>\
                    	<svg xmlns="http://www.w3.org/2000/svg" width="9.954496383666992" height="6.636331558227539" viewBox="1075.0 826.0 9.954496383666992 6.636331558227539" preserveAspectRatio="none">\
                    	  <g>\
                    	    <defs>\
                    	      <path id="s-Path_26-d7263" d="M1075.0 832.6363311718203 L1084.9544967577306 832.6363311718203 L1084.9544967577306 826.0 L1075.0 826.0 L1075.0 832.6363311718203 Z M1076.6590827929551 827.6590827929551 L1083.2954139647754 827.6590827929551 L1083.2954139647754 830.9772483788652 L1076.6590827929551 830.9772483788652 L1076.6590827929551 827.6590827929551 Z "></path>\
                    	    </defs>\
                    	    <g style="mix-blend-mode:normal">\
                    	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_26-d7263" fill="#000000" fill-opacity="1.0"></use>\
                    	    </g>\
                    	  </g>\
                    	</svg>\
\
                    </div>\
                  </div>\
                </div>\
              </div>\
\
              <div id="s-Path_27" class="path firer commentable non-processed" customid="Path"   datasizewidth="44.8px" datasizeheight="9.1px" dataX="1064.0" dataY="838.0"  >\
                <div class="borderLayer">\
                	<div class="imageViewport">\
                  	<?xml version="1.0" encoding="UTF-8"?>\
                  	<svg xmlns="http://www.w3.org/2000/svg" width="44.79523468017578" height="9.124954223632812" viewBox="1064.0 838.0 44.79523468017578 9.124954223632812" preserveAspectRatio="none">\
                  	  <g>\
                  	    <defs>\
                  	      <path id="s-Path_27-d7263" d="M1098.840738652057 838.0 L1091.3748660837591 838.0 L1081.4203693260285 838.0 L1073.9544967577306 838.0 L1064.8295413964775 838.0 L1064.0 838.0 L1064.0 847.124955361253 L1073.9544967577306 847.124955361253 L1073.9544967577306 839.6590827929551 L1081.4203693260285 839.6590827929551 L1081.4203693260285 847.124955361253 L1091.3748660837591 847.124955361253 L1091.3748660837591 839.6590827929551 L1098.840738652057 839.6590827929551 L1098.840738652057 847.124955361253 L1108.7952354097874 847.124955361253 L1108.7952354097874 839.6590827929551 L1108.7952354097874 838.0 L1098.840738652057 838.0 Z M1072.2954139647754 845.4658725682979 L1065.6590827929551 845.4658725682979 L1065.6590827929551 839.6590827929551 L1072.2954139647754 839.6590827929551 L1072.2954139647754 845.4658725682979 Z M1089.715783290804 845.4658725682979 L1083.0794521189837 845.4658725682979 L1083.0794521189837 839.6590827929551 L1089.715783290804 839.6590827929551 L1089.715783290804 845.4658725682979 Z M1107.1361526168325 845.4658725682979 L1100.499821445012 845.4658725682979 L1100.499821445012 839.6590827929551 L1107.1361526168325 839.6590827929551 L1107.1361526168325 845.4658725682979 Z "></path>\
                  	    </defs>\
                  	    <g style="mix-blend-mode:normal">\
                  	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_27-d7263" fill="#000000" fill-opacity="1.0"></use>\
                  	    </g>\
                  	  </g>\
                  	</svg>\
\
                  </div>\
                </div>\
              </div>\
            </div>\
\
          </div>\
\
        </div>\
\
\
        <div id="s-Group_18" class="group firer ie-background commentable non-processed" customid="Product Deliveries" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Path_28" class="path firer commentable non-processed" customid="BG"   datasizewidth="298.9px" datasizeheight="106.3px" dataX="1024.0" dataY="639.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="298.915283203125" height="106.34722900390625" viewBox="1024.0 639.0 298.915283203125 106.34722900390625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_28-d7263" d="M1025.0 655.0 A15.0 15.0 0.0 0 1 1040.0 640.0 L1306.9152777777776 640.0 A15.0 15.0 0.0 0 1 1321.9152777777776 655.0 L1321.9152777777776 729.3472222222222 A15.0 15.0 0.0 0 1 1306.9152777777776 744.3472222222222 L1040.0 744.3472222222222 A15.0 15.0 0.0 0 1 1025.0 729.3472222222222 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_28-d7263" fill="#FFFFFF" fill-opacity="1.0" stroke-width="1.0" stroke="#E7E7E7" stroke-linecap="butt"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_23" class="richtext autofit firer ie-background commentable non-processed" customid="4,591"   datasizewidth="73.6px" datasizeheight="38.0px" dataX="1215.0" dataY="667.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_23_0">4,591</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_24" class="richtext autofit firer ie-background commentable non-processed" customid="Deliveries Made"   datasizewidth="99.4px" datasizeheight="19.0px" dataX="1189.2" dataY="704.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_24_0">Deliveries Made</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Group_19" class="group firer ie-background commentable non-processed" customid="Product Deliveries - icon" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Path_29" class="path firer commentable non-processed" customid="Filled"   datasizewidth="56.8px" datasizeheight="31.8px" dataX="1056.0" dataY="675.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="56.81306838989258" height="31.764694213867188" viewBox="1055.9999998807907 675.0000000097534 56.81306838989258 31.764694213867188" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_29-d7263" d="M1102.856578042377 679.5867431117867 C1102.8068666073675 679.579641478403 1102.7500535350061 679.5725398450192 1102.7003421052882 679.5725398450192 C1102.6790372051369 679.5725398450192 1102.6648339370465 679.579641478403 1102.6506306702788 679.579641478403 C1102.430480028768 679.5654382116354 1102.2174310140267 679.5441333101613 1101.9901787457459 679.5441333101613 L1097.1468645478456 679.5441333101613 L1097.1468645478456 676.6324634971483 C1097.1468645478456 676.09273935469 1096.7065632648234 675.652438071668 1096.1668391223652 675.652438071668 L1075.614711339854 675.652438071668 C1075.5792031716126 675.4251858033871 1075.4797803042395 675.2263400421857 1075.2951378389068 675.1127139292098 C1074.8903447320633 674.8499534847505 1074.2938075331172 675.0630024942005 1073.9529291095312 675.5814217616403 L1071.375036052328 679.5725397312599 L1065.118496603379 679.5725397312599 C1064.5787724609208 679.5725397312599 1064.1384711778987 680.0128410142819 1064.1384711778987 680.5525651567401 C1064.1384711778987 681.0922892991983 1064.5787724609208 681.5325905822203 1065.118496603379 681.5325905822203 L1070.1038433366766 681.5325905822203 L1068.3000284439695 684.3235326118358 L1056.9800254254803 684.3235326118358 C1056.440301283022 684.3235326118358 1056.0 684.7638338948578 1056.0 685.3035580373161 C1056.0 685.8432821797743 1056.440301283022 686.2835834627963 1056.9800254254803 686.2835834627963 L1067.0288366172279 686.2835834627963 L1065.5587983520204 688.5703089335254 L1059.5295123142364 688.5703089335254 C1058.9897881717782 688.5703089335254 1058.549486888756 689.0106102165474 1058.549486888756 689.5503343590057 C1058.549486888756 690.0900585014639 1058.9897881717782 690.5303597844859 1059.5295123142364 690.5303597844859 L1064.287606652266 690.5303597844859 L1062.909889704383 692.6608498472393 C1062.8175684717166 692.802882520206 1062.7678570314163 692.9520168199425 1062.7323488658203 693.0940495034914 C1062.7252472324365 693.0869478701077 1062.7181455990526 693.0798462367239 1062.711043965669 693.0727446033401 L1062.711043965669 693.2360821777809 C1062.7039423322851 693.3142001463252 1062.7039423322851 693.3852164775175 1062.711043965669 693.4562328192919 L1062.711043965669 701.6444161041734 C1062.711043965669 702.2267500495799 1063.314682833714 702.6954578820104 1064.0603543218144 702.6954578820104 L1065.1113960996513 702.6954578820104 C1065.1113960996513 702.6954578820104 1065.1113960996513 702.6954578820104 1065.1113960996513 702.6954578820104 L1067.561459705681 702.6954578820104 C1068.0230658690127 705.0105904100964 1070.05413307233 706.7646939154147 1072.5041966783594 706.7646939154147 C1074.954260284389 706.7646939154147 1076.999530656588 705.0105904100964 1077.4611369045776 702.6954578820104 L1099.4833026824488 702.6954578820104 C1099.9378072190104 705.0105904100964 1101.9830776758674 706.7646939154147 1104.4331411125809 706.7646939154147 C1106.8832047186104 706.7646939154147 1108.9284750908093 705.0105904100964 1109.390081338799 702.6954578820104 L1111.8330433180588 702.6954578820104 C1112.372767460517 702.6954578820104 1112.813068743539 702.2622582257582 1112.813068743539 701.7083308297603 L1112.813068743539 690.3599209113253 C1112.8130658850064 684.6857158734023 1108.4171584065111 680.027044648783 1102.856578042377 679.5867431117867 Z "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_29-d7263" fill="#35B736" fill-opacity="1.0"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
\
            <div id="s-Group_20" class="group firer ie-background commentable non-processed" customid="stroke" datasizewidth="0.0px" datasizeheight="0.0px" >\
              <div id="s-Path_30" class="path firer commentable non-processed" customid="Path 22"   datasizewidth="48.3px" datasizeheight="30.8px" dataX="1062.0" dataY="674.0"  >\
                <div class="borderLayer">\
                	<div class="imageViewport">\
                  	<?xml version="1.0" encoding="UTF-8"?>\
                  	<svg xmlns="http://www.w3.org/2000/svg" width="48.333717346191406" height="30.75717544555664" viewBox="1061.999999716878 674.0 48.333717346191406 30.75717544555664" preserveAspectRatio="none">\
                  	  <g>\
                  	    <defs>\
                  	      <path id="s-Path_30-d7263" d="M1062.8095862665987 700.687940153106 L1065.394580950572 700.687940153106 L1065.422987484107 700.8299728260727 C1065.8703903938988 703.1095972203094 1067.8801527169064 704.7571763240796 1070.2023868717624 704.7571763240796 C1072.5246210266182 704.7571763240796 1074.5343832649678 703.1024957628556 1074.9817862594177 700.8299728260727 L1075.010192792953 700.687940153106 L1097.3235254377046 700.687940153106 L1097.3519319712395 700.8299728260727 C1097.7993348810314 703.1095972203094 1099.809097204039 704.7571763240796 1102.131331358895 704.7571763240796 C1104.4535655137508 704.7571763240796 1106.4633277521004 703.1024957628556 1106.9107307465504 700.8299728260727 L1106.9391372800856 700.687940153106 L1109.5312335908286 700.687940153106 C1109.9786365006205 700.687940153106 1110.333718177746 700.3328584759804 1110.333718177746 699.8783539394186 L1110.333718177746 688.5370433738905 C1110.333718177746 683.033277378444 1106.03012829162 678.3746054765602 1100.5405655497136 677.9343042781962 L1100.526362282946 677.9343042781962 C1100.4837524826432 677.9272026448124 1100.4411426823406 677.9201010114286 1100.3985328741012 677.9201010114286 L1100.334618169679 677.9272026448124 L1100.1641789684684 677.9129993780449 C1100.0079430313797 677.8987961112773 1099.8446054675212 677.8916944778936 1099.688369509268 677.8916944778936 L1094.667514472805 677.8916944778936 L1094.667514472805 674.8024845869174 C1094.667514472805 674.3621833038954 1094.3053311689093 674.0 1093.8650298858875 674.0 L1072.205047118254 674.0 C1071.7647458352321 674.0 1071.4025625313366 674.3621833038954 1071.4025625313366 674.8024845869174 C1071.4025625313366 675.2427858699396 1071.7647458352321 675.6049691738349 1072.205047118254 675.6049691738349 L1093.0554436325165 675.6049691738349 L1093.0554436325165 691.335087199592 L1091.3084417539683 691.335087199592 L1091.2942384872006 691.1575463610292 L1091.2942384872006 691.1575463610292 L1091.2658319536656 691.335087199592 L1062.8095865655473 691.335087199592 C1062.3692852825254 691.335087199592 1062.0071019786299 691.6972705034874 1062.0071019786299 692.1375717865094 C1062.0071019786299 692.5778730695315 1062.3692852825254 692.940056373427 1062.8095865655473 692.940056373427 L1093.8650297218624 692.940056373427 C1094.3053310048845 692.940056373427 1094.6675143087798 692.5778730695315 1094.6675143087798 692.1375717865094 L1094.6675143087798 679.4966641993603 L1099.5960480279184 679.4966641993603 L1099.5960480279184 689.744321617929 C1099.5960480279184 690.184622900951 1099.958231331814 690.5468062048465 1100.3985326148359 690.5468062048465 L1108.7287484351148 690.5468062048465 L1108.7287484351148 699.0758682519462 L1106.9391367959472 699.0758682519462 L1106.910730262412 698.9338355789796 C1106.4633273526201 696.6613128115126 1104.4535650296125 695.0066320809725 1102.1313308747567 695.0066320809725 C1099.8161983466707 695.0066320809725 1097.806435939005 696.6613126421965 1097.3519314871012 698.9338355789796 L1097.3235249535662 699.0758682519462 L1075.0030911132717 699.0758682519462 L1074.9746845797365 698.9338355789796 C1074.5272816699446 696.6613128115126 1072.5104177201672 695.0066320809725 1070.1952851920812 695.0066320809725 C1067.8801526639952 695.0066320809725 1065.8703902563295 696.6613126421965 1065.4158858044257 698.9338355789796 L1065.3874792708907 699.0758682519462 L1062.8024845869174 699.0758682519462 C1062.3621833038953 699.0758682519462 1062.0 699.4380515558416 1062.0 699.8783528388637 C1062.0 700.3186541218857 1062.3621832721487 700.687940153106 1062.8095862665987 700.687940153106 Z M1101.1939150220605 688.9418376236181 L1101.1939150220605 679.6244938198503 L1101.4069640368016 679.667103620153 C1105.6466391126828 680.49089308738 1108.7216467210412 684.2192506125403 1108.7216467210412 688.5370437522062 L1108.7216467210412 688.9418368590499 L1101.1939150220605 688.9418368590499 Z M1102.1242272823322 696.6187031038047 C1103.9209405482698 696.6187031038047 1105.3909786441611 698.0816395729263 1105.3909786441611 699.8854544656335 C1105.3909786441611 701.6892693583409 1103.9209405482698 703.1522058274624 1102.1242272823322 703.1522058274624 C1100.320412389625 703.1522058274624 1098.8574759205035 701.6892693583409 1098.8574759205035 699.8854544656335 C1098.8574769364004 698.0887396758509 1100.3275148629755 696.6187031038047 1102.1242272823322 696.6187031038047 Z M1070.1952848799046 696.6187031038047 C1071.991998145842 696.6187031038047 1073.4620362417334 698.0816395729263 1073.4620362417334 699.8854544656335 C1073.4620362417334 701.6892693583409 1071.991998145842 703.1522058274624 1070.1952848799046 703.1522058274624 C1068.3914699871973 703.1522058274624 1066.9285335180757 701.6892693583409 1066.9285335180757 699.8854544656335 C1066.9285331794435 698.0887396758509 1068.3985711060186 696.6187031038047 1070.1952848799046 696.6187031038047 Z "></path>\
                  	    </defs>\
                  	    <g style="mix-blend-mode:normal">\
                  	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_30-d7263" fill="#000000" fill-opacity="1.0"></use>\
                  	    </g>\
                  	  </g>\
                  	</svg>\
\
                  </div>\
                </div>\
              </div>\
              <div id="s-Path_31" class="path firer commentable non-processed" customid="Path 23"   datasizewidth="14.3px" datasizeheight="1.6px" dataX="1062.0" dataY="678.0"  >\
                <div class="borderLayer">\
                	<div class="imageViewport">\
                  	<?xml version="1.0" encoding="UTF-8"?>\
                  	<svg xmlns="http://www.w3.org/2000/svg" width="14.323994636535645" height="1.6049690246582031" viewBox="1061.9999998807907 678.0 14.323994636535645 1.6049690246582031" preserveAspectRatio="none">\
                  	  <g>\
                  	    <defs>\
                  	      <path id="s-Path_31-d7263" d="M1075.5215101531928 678.0 L1062.8024845869174 678.0 C1062.3621833038953 678.0 1062.0 678.3621833038954 1062.0 678.8024845869174 C1062.0 679.2427858699396 1062.3621833038953 679.604969173835 1062.8024845869174 679.604969173835 L1075.5215101531928 679.604969173835 C1075.9618114362147 679.604969173835 1076.3239947401103 679.2427858699396 1076.3239947401103 678.8024845869174 C1076.3239941475038 678.3550816771256 1075.9689124703782 678.0 1075.5215101531928 678.0 Z "></path>\
                  	    </defs>\
                  	    <g style="mix-blend-mode:normal">\
                  	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_31-d7263" fill="#000000" fill-opacity="1.0"></use>\
                  	    </g>\
                  	  </g>\
                  	</svg>\
\
                  </div>\
                </div>\
              </div>\
              <div id="s-Path_32" class="path firer commentable non-processed" customid="Path 24"   datasizewidth="14.3px" datasizeheight="1.6px" dataX="1056.0" dataY="686.0"  >\
                <div class="borderLayer">\
                	<div class="imageViewport">\
                  	<?xml version="1.0" encoding="UTF-8"?>\
                  	<svg xmlns="http://www.w3.org/2000/svg" width="14.3240966796875" height="1.6049690246582031" viewBox="1055.9999998807907 686.0 14.3240966796875 1.6049690246582031" preserveAspectRatio="none">\
                  	  <g>\
                  	    <defs>\
                  	      <path id="s-Path_32-d7263" d="M1069.5215098145607 686.0 L1056.8024845869174 686.0 C1056.3621833038953 686.0 1056.0 686.3621833038954 1056.0 686.8024845869174 C1056.0 687.2427858699396 1056.3621833038953 687.604969173835 1056.8024845869174 687.604969173835 L1069.521510153193 687.604969173835 C1069.961811436215 687.604969173835 1070.3239947401105 687.2427858699396 1070.3239947401105 686.8024845869174 C1070.3310969594868 686.3621834732116 1069.9689134862751 686.0 1069.5215098145607 686.0 Z "></path>\
                  	    </defs>\
                  	    <g style="mix-blend-mode:normal">\
                  	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_32-d7263" fill="#000000" fill-opacity="1.0"></use>\
                  	    </g>\
                  	  </g>\
                  	</svg>\
\
                  </div>\
                </div>\
              </div>\
              <div id="s-Path_33" class="path firer commentable non-processed" customid="Path 25"   datasizewidth="19.4px" datasizeheight="1.6px" dataX="1054.0" dataY="683.0"  >\
                <div class="borderLayer">\
                	<div class="imageViewport">\
                  	<?xml version="1.0" encoding="UTF-8"?>\
                  	<svg xmlns="http://www.w3.org/2000/svg" width="19.415969848632812" height="1.6049690246582031" viewBox="1054.0 683.0 19.415969848632812 1.6049690246582031" preserveAspectRatio="none">\
                  	  <g>\
                  	    <defs>\
                  	      <path id="s-Path_33-d7263" d="M1072.620482660834 683.0 L1054.8024845869174 683.0 C1054.3621833038953 683.0 1054.0 683.3550816771257 1054.0 683.8024846715757 C1054.0 684.2427859545978 1054.3621833038953 684.6049692584934 1054.8024845869174 684.6049692584934 L1072.6133814573545 684.6049692584934 C1073.0536827403766 684.6049692584934 1073.415866044272 684.2427859545978 1073.415866044272 683.8024846715757 C1073.4229680096742 683.3550816771257 1073.0607845364627 683.0 1072.620482660834 683.0 Z "></path>\
                  	    </defs>\
                  	    <g style="mix-blend-mode:normal">\
                  	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_33-d7263" fill="#000000" fill-opacity="1.0"></use>\
                  	    </g>\
                  	  </g>\
                  	</svg>\
\
                  </div>\
                </div>\
              </div>\
            </div>\
\
          </div>\
\
        </div>\
\
\
        <div id="s-Group_21" class="group firer ie-background commentable non-processed" customid="Inventory Turnover" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Path_34" class="path firer commentable non-processed" customid="BG"   datasizewidth="298.0px" datasizeheight="106.3px" dataX="1024.0" dataY="504.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="297.9666748046875" height="106.34722900390625" viewBox="1024.0 504.0 297.9666748046875 106.34722900390625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_34-d7263" d="M1025.0 520.0 A15.0 15.0 0.0 0 1 1040.0 505.0 L1305.9666666666667 505.0 A15.0 15.0 0.0 0 1 1320.9666666666667 520.0 L1320.9666666666667 594.3472222222222 A15.0 15.0 0.0 0 1 1305.9666666666667 609.3472222222222 L1040.0 609.3472222222222 A15.0 15.0 0.0 0 1 1025.0 594.3472222222222 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_34-d7263" fill="#FFFFFF" fill-opacity="1.0" stroke-width="1.0" stroke="#E7E7E7" stroke-linecap="butt"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_25" class="richtext autofit firer ie-background commentable non-processed" customid="$1.74M"   datasizewidth="97.2px" datasizeheight="38.0px" dataX="1192.0" dataY="532.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_25_0">$1.74M</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_26" class="richtext autofit firer ie-background commentable non-processed" customid="Inventory Sales"   datasizewidth="95.7px" datasizeheight="19.0px" dataX="1193.4" dataY="569.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_26_0">Inventory Sales</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Group_22" class="group firer ie-background commentable non-processed" customid="Inventory Turnover - icon" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Path_35" class="path firer commentable non-processed" customid="Filled"   datasizewidth="40.7px" datasizeheight="39.1px" dataX="1064.0" dataY="538.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="40.73434066772461" height="39.0676383972168" viewBox="1064.0000005351608 537.999999932854 40.73434066772461 39.0676383972168" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_35-d7263" d="M1104.7300270603291 568.3506678901142 L1103.9124133052565 562.6814549545728 C1103.8615112814098 562.3346848987482 1103.5401922612114 562.0929002831066 1103.1902408318592 562.1438022974719 C1103.1902408318592 562.1438022974719 1103.1902408318592 562.1438022974719 1103.1902408318592 562.1438022974719 L1100.9887282270151 562.4619399536239 L1097.9027930230202 541.0480950947333 C1097.877342011097 540.8826635196023 1097.7882634622545 540.7299574385813 1097.6546456603237 540.6281533719257 C1097.5178464659027 540.5263493242326 1097.349233517244 540.4849914304498 1097.1806205496232 540.5104424376324 L1089.3894288923261 541.627103559744 L1089.3480709985433 541.627103559744 L1089.3035317264923 541.627103559744 L1083.0044063774017 542.5337958494374 L1082.9439602234913 542.549702732482 L1082.8771513177853 542.549702732482 L1075.7795003789372 543.5836501149762 L1075.0382402427306 538.5475311028042 C1074.9905195924114 538.2007610469796 1074.6660191986857 537.9557950578104 1074.3192491428608 538.0066970721758 C1074.3192491428608 538.0066970721758 1074.3160677663705 538.0066970721758 1074.3160677663705 538.0066970721758 L1064.5556045332007 539.4001399666797 C1064.2056531038484 539.4446792387307 1063.9606871146793 539.7628168901421 1064.0052263819896 540.1127683194943 C1064.0052263819896 540.1191310724752 1064.0084077584802 540.128675202539 1064.0084077584802 540.1350379555198 L1064.37426605547 542.6546882165159 C1064.4251680793166 543.0014582723406 1064.7464870995148 543.2400615144546 1065.0932571553396 543.1923408736168 L1070.1325576773318 542.4669870266922 L1073.1548654107762 563.4640723327268 C1069.6458071483712 565.3919865108043 1068.366893625008 569.8013744675475 1070.2948079547853 573.3104330333523 C1071.5673585793934 575.6232937875096 1073.9947489322415 577.0612759872488 1076.6352915389834 577.0676388860037 C1076.9852429683356 577.0676388860037 1077.3383757712154 577.0421878740805 1077.6851457891153 576.9912858502339 C1080.4274923487378 576.5936137800438 1082.7021767722647 574.6720623490214 1083.5516043384625 572.0347011916571 L1104.182830854483 569.0601139852756 C1104.3514438031416 569.0378443492501 1104.500968510635 568.9487658075186 1104.6027725772906 568.811966622579 C1104.7045754653095 568.6815289165784 1104.749114542995 568.5160984033464 1104.7300270603291 568.3506678901142 Z "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_35-d7263" fill="#35B736" fill-opacity="1.0"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
\
            <div id="s-Group_23" class="group firer ie-background commentable non-processed" customid="Stroke" datasizewidth="0.0px" datasizeheight="0.0px" >\
              <div id="s-Path_36" class="path firer commentable non-processed" customid="Path"   datasizewidth="7.2px" datasizeheight="4.7px" dataX="1086.0" dataY="556.0"  >\
                <div class="borderLayer">\
                	<div class="imageViewport">\
                  	<?xml version="1.0" encoding="UTF-8"?>\
                  	<svg xmlns="http://www.w3.org/2000/svg" width="7.1714935302734375" height="4.675331115722656" viewBox="1085.999999187796 555.9999995566262 7.1714935302734375 4.675331115722656" preserveAspectRatio="none">\
                  	  <g>\
                  	    <defs>\
                  	      <path id="s-Path_36-d7263" d="M1086.3852809675693 560.1257456448645 C1086.4298202396203 560.440701927489 1086.7034186237213 560.6761237960754 1087.0215562798735 560.672942422548 C1087.0533700459628 560.6761237990384 1087.0820024337838 560.6761237990384 1087.1138161975027 560.672942422548 L1092.623960304968 559.8807796556954 C1092.9707303607927 559.8330590053764 1093.2156963499617 559.5085586116505 1093.1647943355965 559.1617885558259 C1093.1647943355965 559.1617885558259 1093.1647943355965 559.1586071793355 1093.1647943355965 559.1586071793355 L1092.7798477595165 556.543515560814 C1092.757578123491 556.3780839856829 1092.6684995817595 556.2253779046619 1092.5317003968198 556.1267552494587 C1092.3980825854078 556.0249512017656 1092.2262882537404 555.9835933079828 1092.0608566596468 556.005862941638 L1086.5475311028042 556.7980257084905 C1086.2007610469796 556.8457463588096 1085.9557950578105 557.1702467525354 1086.0066970721757 557.51701680836 C1086.0066970721757 557.51701680836 1086.0066970721757 557.5201981848505 1086.0066970721757 557.5201981848505 L1086.3852809675693 560.1257456448645 Z M1091.6091006990582 557.3452232097112 L1091.803164673862 558.7004896977347 L1087.55284558312 559.3240373921307 L1087.3715071213887 557.9687709041071 L1091.6091006990582 557.3452232097112 Z "></path>\
                  	    </defs>\
                  	    <g style="mix-blend-mode:normal">\
                  	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_36-d7263" fill="#000000" fill-opacity="1.0"></use>\
                  	    </g>\
                  	  </g>\
                  	</svg>\
\
                  </div>\
                </div>\
              </div>\
              <div id="s-Path_37" class="path firer commentable non-processed" customid="Path"   datasizewidth="40.7px" datasizeheight="39.1px" dataX="1060.0" dataY="537.0"  >\
                <div class="borderLayer">\
                	<div class="imageViewport">\
                  	<?xml version="1.0" encoding="UTF-8"?>\
                  	<svg xmlns="http://www.w3.org/2000/svg" width="40.73416519165039" height="39.0676383972168" viewBox="1060.0000000003488 536.9999999994436 40.73416519165039 39.0676383972168" preserveAspectRatio="none">\
                  	  <g>\
                  	    <defs>\
                  	      <path id="s-Path_37-d7263" d="M1100.7300273637288 567.3506672074649 L1099.9124136086562 561.6814542719235 C1099.8615115848095 561.3346842160987 1099.5401925646113 561.0928996004571 1099.190241135259 561.1438016148226 C1099.190241135259 561.1438016148226 1099.190241135259 561.1438016148226 1099.190241135259 561.1438016148226 L1096.9887285304148 561.4619392709745 L1093.90279332642 540.048094412084 C1093.8773423144967 539.8826628369529 1093.7882637656542 539.729956755932 1093.6546459637234 539.6281526892764 C1093.5178467693024 539.5263486415831 1093.349233820644 539.4849907478005 1093.180620853023 539.510441754983 L1085.3894291957258 540.6271034838941 L1085.348071301943 540.6271034838941 L1085.303532029892 540.6271034838941 L1079.0044066808014 541.5337957735875 L1078.943960526891 541.5497026566321 L1078.877151621185 541.5497026566321 L1071.779500682337 542.5836500391262 L1071.0382405461303 537.5475311028042 C1070.990519895811 537.2007610469796 1070.6660195020854 536.9557950578104 1070.3192494462605 537.0066970721758 C1070.3192494462605 537.0066970721758 1070.3160680697702 537.0066970721758 1070.3160680697702 537.0066970721758 L1060.5556045332007 538.4001400425296 C1060.2056531038484 538.4446793145806 1059.9606871146793 538.762816965992 1060.0052263819896 539.1127683953442 C1060.0052263819896 539.1191311483251 1060.0084077584802 539.1286752783889 1060.0084077584802 539.1350380313697 L1060.37426605547 541.6546882923658 C1060.4251680793166 542.0014583481906 1060.7464870995148 542.2400615903045 1061.0932571553396 542.1923409494667 L1066.1325576773318 541.4669871025421 L1069.1548654107762 562.4640724085767 C1065.6458071483712 564.3919865866542 1064.366893625008 568.8013745433974 1066.2948079547853 572.3104331092022 C1067.5673585793934 574.6232938633595 1069.9947489322415 576.0612760630987 1072.6352915389834 576.0676389618536 C1072.9852429683356 576.0676389618536 1073.3383757712154 576.0421879499304 1073.6851457891153 575.9912859260838 C1076.4274923487378 575.5936138558938 1078.7021767722647 573.6720624248715 1079.5516043384628 571.034701267507 L1100.182830854483 568.0601140611255 C1100.3514438031416 568.0378444251 1100.500968510635 567.9487658833685 1100.6027725772906 567.8119666984289 C1100.7013961397302 567.6815306611268 1100.7491148463948 567.516097720697 1100.7300273637288 567.3506672074649 Z M1084.8549578363024 541.9855514212952 L1086.2770330925541 551.8478187620084 L1083.9069076149015 550.4607385387096 C1083.6492161126598 550.3112138406974 1083.3247156904902 550.3589344910164 1083.121107595104 550.5752680994754 L1081.2377326464118 552.5731726468581 L1079.8156573901601 542.7109053061448 L1084.8549578363024 541.9855514212952 Z M1078.5558337008106 542.8922438626884 L1080.1687916721135 554.0716009056953 C1080.2037868141006 554.3133855213368 1080.3723997674997 554.5138122431956 1080.6046402625589 554.5869839101786 C1080.6682677947372 554.606072169121 1080.7318953269157 554.61561630037 1080.7987042373625 554.61561630037 C1080.9736799520385 554.61561630037 1081.1391115461322 554.5424446381277 1081.2600038539529 554.4151895785112 L1083.700119700911 551.8255489482098 L1086.773329410796 553.6198452864312 C1087.0755601803478 553.7980023746348 1087.4668695224452 553.6993797004693 1087.6450265916865 553.3971489309173 C1087.7181982539287 553.273075249569 1087.7468306393796 553.126731915603 1087.727742379252 552.9835699930896 L1086.1147826634008 541.8042130543764 L1092.725683376688 540.8498000859203 L1095.7225401219123 561.6241900035276 L1078.2026994947073 564.169291252744 C1077.2514678997786 563.027177094464 1075.9693731545879 562.2095633393914 1074.534572404226 561.8246167633114 L1071.9449317739245 543.8434762919662 L1078.5558337008106 542.8922438626884 Z M1067.3064853782992 540.6652802696241 C1067.281034366376 540.499848694493 1067.1919558175334 540.3471426134721 1067.0583380156027 540.2453385468165 C1066.9215388211817 540.1467158726508 1066.752925872523 540.1021766053404 1066.5874942784296 540.1276276125232 L1061.5481939839872 540.8434371871654 L1061.366855522256 539.5708865625572 L1069.8706749255682 538.358782110822 L1073.2174832139194 561.5828310099209 C1072.2535261248809 561.506477974151 1071.2863875864646 561.6305516602399 1070.373332701416 561.9423265598557 L1067.3064853782992 540.6652802696241 Z M1073.5038073813087 574.7314598593132 C1070.2365335069953 575.2023035964862 1067.204682032218 572.9371635210916 1066.7338378399454 569.6667083491008 C1066.2629941027724 566.3994344747874 1068.528134178167 563.3675830000101 1071.7954079007804 562.8967388077375 C1072.0785504102046 562.8553809139547 1072.368055666684 562.833111275559 1072.654379587561 562.833111275559 C1075.956648494827 562.8458367815206 1078.6258235391665 565.5309184540964 1078.6162791910342 568.8331876647622 C1078.603553621074 571.7950506522606 1076.430674060627 574.3019745089474 1073.5038073813087 574.7314598593132 Z M1079.8442909655066 569.7048863484296 C1079.9238253795447 569.0654296625979 1079.9206440012765 568.4196102297113 1079.828384082462 567.7801534680298 C1079.701129018105 566.9211817812493 1079.4211678916895 566.0908425320664 1079.0044075803341 565.3273121269615 L1096.5337919488722 562.8012991189104 L1096.5337919488722 562.8012991189104 L1098.7353045537163 562.4831614627583 L1099.3715798660203 566.8925492678017 L1079.8442909655066 569.7048863484296 Z "></path>\
                  	    </defs>\
                  	    <g style="mix-blend-mode:normal">\
                  	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_37-d7263" fill="#000000" fill-opacity="1.0"></use>\
                  	    </g>\
                  	  </g>\
                  	</svg>\
\
                  </div>\
                </div>\
              </div>\
            </div>\
\
          </div>\
\
        </div>\
\
      </div>\
\
\
      <div id="s-Group_24" class="group firer ie-background commentable non-processed" customid="Sales" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Path_38" class="path firer commentable non-processed" customid="BG"   datasizewidth="748.6px" datasizeheight="315.0px" dataX="574.0" dataY="155.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="748.5569458007812" height="315.0416564941406" viewBox="574.0 155.0 748.5569458007812 315.0416564941406" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_38-d7263" d="M575.0 176.0 A20.0 20.0 0.0 0 1 595.0 156.0 L1301.5569444444445 156.0 A20.0 20.0 0.0 0 1 1321.5569444444445 176.0 L1321.5569444444445 449.04166666666663 A20.0 20.0 0.0 0 1 1301.5569444444445 469.04166666666663 L595.0 469.04166666666663 A20.0 20.0 0.0 0 1 575.0 449.04166666666663 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_38-d7263" fill="#FFFFFF" fill-opacity="1.0" stroke-width="1.0" stroke="#E7E7E7" stroke-linecap="butt"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_25" class="group firer ie-background commentable non-processed" customid="Graphics" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Path_39" class="path firer commentable non-processed" customid="graphic-2"   datasizewidth="607.1px" datasizeheight="123.3px" dataX="666.0" dataY="290.7"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="607.111083984375" height="123.3328857421875" viewBox="666.0 290.68280029296875 607.111083984375 123.3328857421875" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_39-d7263" d="M666.0 414.0156792534722 C690.6450501230028 362.4747264438205 708.0246021034827 289.24562763774713 717.8144804000855 290.7042500389947 C744.6613964716594 294.7042500389947 768.1008500417074 356.08294957478836 805.109884262085 356.08294957478836 C842.1189184824625 356.08294957478836 826.8092013888888 389.28109609815806 874.5124884287516 381.2775661892361 C922.2157754686143 373.27403628031414 919.0629404703775 308.0 945.6715110778808 306.0 C972.2800816853842 304.0 962.0664563669459 385.317256349273 1010.7625240749783 381.2775661892361 C1082.803543044382 375.30124388751295 1074.497610272258 329.8728359293593 1117.103790367974 356.0829495747884 C1182.1262937757704 396.0829495747884 1213.3328433566621 336.0 1236.0046720716687 336.0 C1251.1192245483398 336.0 1256.8213322957356 342.00523124270967 1273.111111111111 414.0156792534722 L666.0 414.0156792534722 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_39-d7263" fill="#F1F1F1" fill-opacity="0.57"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_40" class="path firer ie-background commentable non-processed" customid="graphic-2"   datasizewidth="613.1px" datasizeheight="129.3px" dataX="663.1" dataY="288.7"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="612.3682250976562" height="127.3328857421875" viewBox="663.1200561523438 288.68280029296875 612.3682250976562 127.3328857421875" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_40-d7263" d="M666.0 414.0156792534722 C690.6450501230028 362.4747264438205 708.0246021034827 289.24562763774713 717.8144804000855 290.7042500389947 C744.6613964716594 294.7042500389947 768.1008500417074 356.08294957478836 805.109884262085 356.08294957478836 C842.1189184824625 356.08294957478836 826.8092013888888 389.28109609815806 874.5124884287516 381.2775661892361 C922.2157754686143 373.27403628031414 919.0629404703775 308.0 945.6715110778808 306.0 C972.2800816853842 304.0 962.0664563669459 385.317256349273 1010.7625240749783 381.2775661892361 C1082.803543044382 375.30124388751295 1074.497610272258 329.8728359293593 1117.103790367974 356.0829495747884 C1182.1262937757704 396.0829495747884 1213.3328433566621 336.0 1236.0046720716687 336.0 C1251.1192245483398 336.0 1256.8213322957356 342.00523124270967 1273.111111111111 414.0156792534722 L666.0 414.0156792534722 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_40-d7263" fill="none" stroke-width="3.0" stroke="#E2E2E2" stroke-linecap="butt"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_41" class="path firer ie-background commentable non-processed" customid="graphic-1"   datasizewidth="607.1px" datasizeheight="154.8px" dataX="666.0" dataY="259.2"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="607.111083984375" height="154.82907104492188" viewBox="666.0 259.1820373535156 607.111083984375 154.82907104492188" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_41-d7263" d="M666.0 414.01111111111106 C688.1532596588135 397.715817451477 692.549382824368 367.56816338433157 711.1883839713203 367.56816338433157 C739.1468784544203 367.56816338433157 766.0138052198622 359.7824327256944 802.7778308232625 315.7824327256944 C835.3270294301867 276.82683101454217 845.8325872251138 359.32886386471677 850.0382619645861 367.5681633843316 C862.2888414594862 391.5681633843316 875.0029631296793 386.73393872578936 899.5756037818061 386.73393872578936 C927.121252102322 386.73393872578936 963.4448321448432 315.78243272569443 983.6888556162517 315.78243272569443 C1009.3616570366753 315.78243272569443 1026.9229340871175 379.1056787702772 1050.0125882466632 379.1056787702772 C1073.102271355523 379.1056787702772 1087.2426215277778 259.1820372687446 1110.8085208468965 259.1820372687446 C1131.7364429050021 259.1820372687446 1146.5392112731934 380.90762880113385 1161.546651289198 386.73393872578936 C1177.381115298801 392.8812965393066 1214.6941854576453 342.85671290684434 1227.4453269110786 344.97587477366125 C1239.0532144572126 346.90503482038343 1253.8474850972493 367.3209487279256 1273.111111111111 414.01111111111106 L666.0 414.01111111111106 Z "></path>\
              	      <linearGradient id="s-Path_41-d7263-gradient" x1="1273.111111111111" y1="336.5965741899278" x2="666.0" y2="336.5965741899278" gradientUnits="userSpaceOnUse">\
              	        <stop stop-color="#E0C21D" offset="0.0%" stop-opacity="0.8"></stop>\
              	        <stop stop-color="#FFE03A" offset="100.0%" stop-opacity="1.0"></stop>\
              	      </linearGradient>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_41-d7263" fill="url(#s-Path_41-d7263-gradient)" fill-opacity="0.2"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Path_42" class="path firer commentable non-processed" customid="graphic-1"   datasizewidth="613.1px" datasizeheight="160.8px" dataX="660.9" dataY="257.2"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="614.92333984375" height="158.82907104492188" viewBox="660.9292602539062 257.1820373535156 614.92333984375 158.82907104492188" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_42-d7263" d="M666.0 414.01111111111106 C688.1532596588135 397.715817451477 692.549382824368 367.56816338433157 711.1883839713203 367.56816338433157 C739.1468784544203 367.56816338433157 766.0138052198622 359.7824327256944 802.7778308232625 315.7824327256944 C835.3270294301867 276.82683101454217 845.8325872251138 359.32886386471677 850.0382619645861 367.5681633843316 C862.2888414594862 391.5681633843316 875.0029631296793 386.73393872578936 899.5756037818061 386.73393872578936 C927.121252102322 386.73393872578936 963.4448321448432 315.78243272569443 983.6888556162517 315.78243272569443 C1009.3616570366753 315.78243272569443 1026.9229340871175 379.1056787702772 1050.0125882466632 379.1056787702772 C1073.102271355523 379.1056787702772 1087.2426215277778 259.1820372687446 1110.8085208468965 259.1820372687446 C1131.7364429050021 259.1820372687446 1146.5392112731934 380.90762880113385 1161.546651289198 386.73393872578936 C1177.381115298801 392.8812965393066 1214.6941854576453 342.85671290684434 1227.4453269110786 344.97587477366125 C1239.0532144572126 346.90503482038343 1253.8474850972493 367.3209487279256 1273.111111111111 414.01111111111106 L666.0 414.01111111111106 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_42-d7263" fill="#A7F9A8" fill-opacity="0.38" stroke-width="3.0" stroke="#35B736" stroke-linecap="butt"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_26" class="group firer ie-background commentable non-processed" customid="Tooltip" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Path_43" class="path firer commentable non-processed" customid="BG tooltip"   datasizewidth="98.0px" datasizeheight="30.8px" dataX="1064.0" dataY="216.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="97.97412109375" height="30.765716552734375" viewBox="1064.0 216.00000000000006 97.97412109375 30.765716552734375" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_43-d7263" d="M1066.1993531672524 240.19288483977533 C1064.9846839589163 240.19288483977533 1064.0 239.20820186404654 1064.0 237.9935316725231 L1064.0 237.9935316725231 L1064.0 218.19935316725235 C1064.0 216.98468395891632 1064.9846839589163 216.00000000000006 1066.1993531672524 216.00000000000006 L1066.1993531672524 216.00000000000006 L1159.7747735228397 216.00000000000006 C1160.9894416168968 216.00000000000006 1161.974126690092 216.98468395891632 1161.974126690092 218.19935316725235 L1161.974126690092 218.19935316725235 L1161.974126690092 237.9935316725231 C1161.974126690092 239.20820186404654 1160.9894416168968 240.19288483977533 1159.7747735228397 240.19288483977533 L1159.7747735228397 240.19288483977533 L1114.585122846803 240.19288483977533 L1107.987063345046 246.76570762306028 L1101.3890038432892 240.19288483977533 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_43-d7263" fill="#35B736" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_27" class="richtext autofit firer commentable non-processed" customid="2,478,369"   datasizewidth="81.3px" datasizeheight="19.0px" dataX="1071.7" dataY="218.7" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_27_0">$ 2,478,369</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_27" class="group firer ie-background commentable non-processed" customid="Units" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Paragraph_28" class="richtext autofit firer ie-background commentable non-processed" customid="0.5M"   datasizewidth="30.0px" datasizeheight="16.0px" dataX="620.0" dataY="396.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_28_0">0,.5M</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_29" class="richtext autofit firer ie-background commentable non-processed" customid="1.0M"   datasizewidth="27.2px" datasizeheight="16.0px" dataX="620.0" dataY="359.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_29_0">1.0M</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_30" class="richtext autofit firer ie-background commentable non-processed" customid="1.5M"   datasizewidth="27.2px" datasizeheight="16.0px" dataX="620.0" dataY="322.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_30_0">1.5M</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_31" class="richtext autofit firer ie-background commentable non-processed" customid="2.0M"   datasizewidth="27.2px" datasizeheight="16.0px" dataX="620.0" dataY="285.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_31_0">2.0M</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_32" class="richtext autofit firer ie-background commentable non-processed" customid="2.5M"   datasizewidth="27.2px" datasizeheight="16.0px" dataX="620.0" dataY="248.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_32_0">2.5M</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_33" class="richtext autofit firer ie-background commentable non-processed" customid="JAN"   datasizewidth="21.5px" datasizeheight="16.0px" dataX="667.0" dataY="419.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_33_0">JAN</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_34" class="richtext autofit firer ie-background commentable non-processed" customid="FEB"   datasizewidth="21.8px" datasizeheight="16.0px" dataX="772.0" dataY="419.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_34_0">FEB</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_35" class="richtext autofit firer ie-background commentable non-processed" customid="MAR"   datasizewidth="27.0px" datasizeheight="16.0px" dataX="887.0" dataY="419.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_35_0">MAR</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_36" class="richtext autofit firer ie-background commentable non-processed" customid="APR"   datasizewidth="24.7px" datasizeheight="16.0px" dataX="1018.0" dataY="419.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_36_0">APR</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_37" class="richtext autofit firer ie-background commentable non-processed" customid="MAY"   datasizewidth="26.0px" datasizeheight="16.0px" dataX="1139.0" dataY="419.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_37_0">MAY</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_38" class="richtext autofit firer ie-background commentable non-processed" customid="JUN"   datasizewidth="21.5px" datasizeheight="16.0px" dataX="1264.0" dataY="419.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_38_0">JUN</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_28" class="group firer ie-background commentable non-processed" customid="Dropdown" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Group_29" class="group firer ie-background commentable non-processed" customid="arrow icon" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Path_44" class="path firer ie-background commentable non-processed" customid="frame"   datasizewidth="9.5px" datasizeheight="9.5px" dataX="488.0" dataY="-5.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="-1.0" height="-1.0" viewBox="488.0 -5.0 -1.0 -1.0" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_44-d7263" d="M1275.0 193.0 L1284.486111111111 193.0 L1284.486111111111 202.48611111111114 L1275.0 202.48611111111114 Z "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_44-d7263" fill="none"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_45" class="path firer commentable non-processed" customid="Shape"   datasizewidth="9.1px" datasizeheight="5.7px" dataX="1274.8" dataY="201.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="9.48602294921875" height="5.6916656494140625" viewBox="1274.783563748002 200.99999996434158 9.48602294921875 5.6916656494140625" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_45-d7263" d="M1283.1871826728807 201.2543368756271 L1279.5265672869773 205.0729196196957 L1275.8659516749076 201.2543368756271 C1275.5773013474834 200.9222860989799 1275.2886503415598 200.91536811767318 1275.0 201.23358361020652 C1274.7113489799408 201.55179898965656 1274.7113489799408 201.8630963877999 1275.0 202.16747580463655 L1279.0935914000497 206.48413488330084 C1279.19855527884 206.62248953377178 1279.3428802729272 206.69166663284074 1279.5265672869773 206.69166663284074 C1279.7102543010274 206.69166663284074 1279.8545792951145 206.62248953377178 1279.9595431739049 206.48413488330084 L1284.0531344467358 202.16747580463655 C1284.3417853395765 201.8630963877999 1284.3417853395765 201.55179898965656 1284.0531344467358 201.23358361020652 C1283.7644844585616 200.91536811767318 1283.4758335657211 200.9222860989799 1283.1871826728807 201.2543368756271 Z "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_45-d7263" fill="#2B3034" fill-opacity="1.0"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Paragraph_39" class="richtext autofit firer ie-background commentable non-processed" customid="Paragraph"   datasizewidth="76.3px" datasizeheight="16.0px" dataX="1191.0" dataY="195.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_39_0">Last 6 Months</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Paragraph_40" class="richtext autofit firer ie-background commentable non-processed" customid="Sales"   datasizewidth="269.5px" datasizeheight="34.0px" dataX="613.0" dataY="192.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_40_0">Ventas Totales (copias)</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_1" class="rectangle manualfit firer commentable non-processed" customid="Rectangle"   datasizewidth="627.0px" datasizeheight="8.0px" datasizewidthpx="627.0" datasizeheightpx="8.0" dataX="658.0" dataY="411.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_1_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_30" class="group firer ie-background commentable non-processed" customid="Outlay" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Path_46" class="path firer commentable non-processed" customid="BG"   datasizewidth="498.1px" datasizeheight="315.0px" dataX="46.0" dataY="155.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="498.1236572265625" height="315.0416564941406" viewBox="46.0 155.0 498.1236572265625 315.0416564941406" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_46-d7263" d="M47.0 176.0 A20.0 20.0 0.0 0 1 67.0 156.0 L523.1236111111111 156.0 A20.0 20.0 0.0 0 1 543.1236111111111 176.0 L543.1236111111111 449.04166666666663 A20.0 20.0 0.0 0 1 523.1236111111111 469.04166666666663 L67.0 469.04166666666663 A20.0 20.0 0.0 0 1 47.0 449.04166666666663 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_46-d7263" fill="#FFFFFF" fill-opacity="1.0" stroke-width="1.0" stroke="#E7E7E7" stroke-linecap="butt"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_41" class="richtext manualfit firer ie-background commentable non-processed" customid="Outlay"   datasizewidth="133.4px" datasizeheight="68.0px" dataX="79.0" dataY="182.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_41_0">Lectores por genero</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_31" class="group firer ie-background commentable non-processed" customid="Pie" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Group_32" class="group firer ie-background commentable non-processed" customid="Pie" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Path_47" class="path firer commentable non-processed" customid="Subtraction 1"   datasizewidth="114.4px" datasizeheight="209.6px" dataX="258.0" dataY="196.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="114.3997802734375" height="209.577392578125" viewBox="257.99999764324053 196.0 114.3997802734375 209.577392578125" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_47-d7263" d="M372.3993394306302 196.0 C309.36861168567737 196.64990107306312 257.7183931913167 248.1126666639734 258.0011555473951 310.42636318757513 C257.82226910211006 349.84769724377384 278.42768516662323 384.95605145146305 309.5848733172859 405.57738564433816 L351.37964709938905 342.606537085288 C340.8144997571033 335.5042194287744 333.84470518742796 323.52710661596325 333.9056753746155 310.09107331579025 C333.80975957704266 288.9531751914951 351.11503732622685 271.4513661493229 372.399641554063 270.8118044115131 L372.3997758311442 270.8118044115131 L372.3997758311442 196.0 Z "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_47-d7263" fill="#35B736" fill-opacity="1.0"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_48" class="path firer commentable non-processed" customid="Intersection 2"   datasizewidth="106.1px" datasizeheight="101.2px" dataX="376.0" dataY="196.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="106.12689208984375" height="101.202392578125" viewBox="376.0 195.9999999999999 106.12689208984375 101.202392578125" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_48-d7263" d="M376.0 196.0 L376.0 270.84349380267884 C392.29405383521646 271.78195632326526 406.06386639001056 282.5865616629386 411.2199720314462 297.2023533986347 L482.1268982135216 270.6122686688328 C466.0035105485248 227.92840437044117 424.4900052899716 196.9547771859534 376.00036926197333 196.0 Z "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_48-d7263" fill="#09E00A" fill-opacity="1.0"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_49" class="path firer commentable non-processed" customid="Intersection 3"   datasizewidth="89.1px" datasizeheight="88.5px" dataX="400.0" dataY="274.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="89.10479736328125" height="88.4708251953125" viewBox="400.0 274.0 89.10479736328125 88.4708251953125" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_49-d7263" d="M484.5475977540145 274.0 L411.9952402187748 300.5030731811771 C412.0675484270071 301.48786129478117 412.10215834468937 311.28216587542033 412.0975929239282 312.2846785637446 C412.1475104288685 323.28546271906794 407.4843359525598 332.2014609115033 400.0 339.36823156699825 L477.0087117372126 362.47087194357823 C484.8152791154094 347.08701530335316 489.18643450935366 329.76876303119684 489.10361911951657 311.5199705862458 C489.15393945570054 300.4297578948356 487.5593319777748 284.181022574528 484.5476313232848 274.0 Z "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_49-d7263" fill="#9CEE9D" fill-opacity="1.0"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
            <div id="s-Path_50" class="path firer commentable non-processed" customid="Intersection 4"   datasizewidth="161.6px" datasizeheight="83.8px" dataX="313.0" dataY="341.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="161.5797119140625" height="83.8092041015625" viewBox="313.0 341.0 161.5797119140625 83.8092041015625" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_50-d7263" d="M397.9507982596219 341.0 C391.20236785064105 346.21545611133115 382.7268991923898 349.338875297389 373.5711828407217 349.338875297389 C373.5120673557184 349.338875297389 373.4529518707151 349.3387410203079 373.3937692471711 349.3384724661453 C365.99339003963877 349.3058431354111 359.04770659835845 347.2360962056189 353.10594575475494 343.67547084313674 L313.0 407.72523574972047 C330.51597527424 418.4562572488901 351.09830197502305 424.71128652295965 373.05505530981543 424.8081002985132 C373.22739994355027 424.8089059610004 373.3996438694741 424.8091745151628 373.57178708758715 424.8091745151628 C416.75532997443656 424.8091745151628 454.74211483351354 400.94746579849493 474.57970727262796 365.9877563197065 L397.9507982596219 341.0 Z "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_50-d7263" fill="#C6D2C6" fill-opacity="1.0"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Paragraph_42" class="richtext autofit firer ie-background commentable non-processed" customid="6%"   datasizewidth="21.0px" datasizeheight="21.0px" dataX="438.0" dataY="307.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_42_0">4%</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_43" class="richtext autofit firer ie-background commentable non-processed" customid="70%"   datasizewidth="28.8px" datasizeheight="21.0px" dataX="284.0" dataY="283.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_43_0">72%</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_44" class="richtext autofit firer ie-background commentable non-processed" customid="9%"   datasizewidth="26.5px" datasizeheight="21.0px" dataX="373.0" dataY="370.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_44_0">18%</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div id="s-Paragraph_45" class="richtext autofit firer ie-background commentable non-processed" customid="15%"   datasizewidth="20.8px" datasizeheight="21.0px" dataX="406.0" dataY="236.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_45_0">6%</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_33" class="group firer ie-background commentable non-processed" customid="Variants" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Group_34" class="group firer ie-background commentable non-processed" customid="Variant 1" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="shapewrapper-s-Ellipse_1" customid="Ellipse 5" class="shapewrapper shapewrapper-s-Ellipse_1 non-processed"   datasizewidth="16.0px" datasizeheight="16.4px" datasizewidthpx="16.0" datasizeheightpx="16.363636363636374" dataX="83.8" dataY="258.0" >\
                <div class="backgroundLayer">\
                  <div class="colorLayer"></div>\
                  <div class="imageLayer"></div>\
                </div>\
                <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_1" class="svgContainer" style="width:100%; height:100%;">\
                    <g>\
                        <g clip-path="url(#clip-s-Ellipse_1)">\
                                <ellipse id="s-Ellipse_1" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 5" cx="8.0" cy="8.181818181818187" rx="8.0" ry="8.181818181818187">\
                                </ellipse>\
                        </g>\
                    </g>\
                    <defs>\
                        <clipPath id="clip-s-Ellipse_1" class="clipPath">\
                                <ellipse cx="8.0" cy="8.181818181818187" rx="8.0" ry="8.181818181818187">\
                                </ellipse>\
                        </clipPath>\
                    </defs>\
                </svg>\
                <div class="paddingLayer">\
                    <div id="shapert-s-Ellipse_1" class="content firer" >\
                        <div class="valign">\
                            <span id="rtr-s-Ellipse_1_0"></span>\
                        </div>\
                    </div>\
                </div>\
            </div>\
            <div id="s-Paragraph_46" class="richtext manualfit firer ie-background commentable non-processed" customid="Consumer-Base"   datasizewidth="57.4px" datasizeheight="19.0px" dataX="110.8" dataY="256.8" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_46_0">Accion</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
\
          <div id="s-Group_35" class="group firer ie-background commentable non-processed" customid="Variant 2" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="shapewrapper-s-Ellipse_2" customid="Ellipse 5" class="shapewrapper shapewrapper-s-Ellipse_2 non-processed"   datasizewidth="16.0px" datasizeheight="16.4px" datasizewidthpx="16.0" datasizeheightpx="16.363636363636374" dataX="83.8" dataY="298.0" >\
                <div class="backgroundLayer">\
                  <div class="colorLayer"></div>\
                  <div class="imageLayer"></div>\
                </div>\
                <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_2" class="svgContainer" style="width:100%; height:100%;">\
                    <g>\
                        <g clip-path="url(#clip-s-Ellipse_2)">\
                                <ellipse id="s-Ellipse_2" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 5" cx="8.0" cy="8.181818181818187" rx="8.0" ry="8.181818181818187">\
                                </ellipse>\
                        </g>\
                    </g>\
                    <defs>\
                        <clipPath id="clip-s-Ellipse_2" class="clipPath">\
                                <ellipse cx="8.0" cy="8.181818181818187" rx="8.0" ry="8.181818181818187">\
                                </ellipse>\
                        </clipPath>\
                    </defs>\
                </svg>\
                <div class="paddingLayer">\
                    <div id="shapert-s-Ellipse_2" class="content firer" >\
                        <div class="valign">\
                            <span id="rtr-s-Ellipse_2_0"></span>\
                        </div>\
                    </div>\
                </div>\
            </div>\
            <div id="s-Paragraph_47" class="richtext autofit firer ie-background commentable non-processed" customid="Consumer-Base"   datasizewidth="58.8px" datasizeheight="19.0px" dataX="110.8" dataY="296.8" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_47_0">Romance</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
\
          <div id="s-Group_36" class="group firer ie-background commentable non-processed" customid="Variant 3" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="shapewrapper-s-Ellipse_3" customid="Ellipse 5" class="shapewrapper shapewrapper-s-Ellipse_3 non-processed"   datasizewidth="16.0px" datasizeheight="16.4px" datasizewidthpx="16.0" datasizeheightpx="16.363636363636374" dataX="83.8" dataY="338.0" >\
                <div class="backgroundLayer">\
                  <div class="colorLayer"></div>\
                  <div class="imageLayer"></div>\
                </div>\
                <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_3" class="svgContainer" style="width:100%; height:100%;">\
                    <g>\
                        <g clip-path="url(#clip-s-Ellipse_3)">\
                                <ellipse id="s-Ellipse_3" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 5" cx="8.0" cy="8.181818181818187" rx="8.0" ry="8.181818181818187">\
                                </ellipse>\
                        </g>\
                    </g>\
                    <defs>\
                        <clipPath id="clip-s-Ellipse_3" class="clipPath">\
                                <ellipse cx="8.0" cy="8.181818181818187" rx="8.0" ry="8.181818181818187">\
                                </ellipse>\
                        </clipPath>\
                    </defs>\
                </svg>\
                <div class="paddingLayer">\
                    <div id="shapert-s-Ellipse_3" class="content firer" >\
                        <div class="valign">\
                            <span id="rtr-s-Ellipse_3_0"></span>\
                        </div>\
                    </div>\
                </div>\
            </div>\
            <div id="s-Paragraph_48" class="richtext autofit firer ie-background commentable non-processed" customid="Consumer-Base"   datasizewidth="42.1px" datasizeheight="19.0px" dataX="110.8" dataY="336.8" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_48_0">Drama</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
\
          <div id="s-Group_37" class="group firer ie-background commentable non-processed" customid="Variant 4" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="shapewrapper-s-Ellipse_4" customid="Ellipse 5" class="shapewrapper shapewrapper-s-Ellipse_4 non-processed"   datasizewidth="16.0px" datasizeheight="16.4px" datasizewidthpx="16.0" datasizeheightpx="16.363636363636374" dataX="83.8" dataY="378.0" >\
                <div class="backgroundLayer">\
                  <div class="colorLayer"></div>\
                  <div class="imageLayer"></div>\
                </div>\
                <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-s-Ellipse_4" class="svgContainer" style="width:100%; height:100%;">\
                    <g>\
                        <g clip-path="url(#clip-s-Ellipse_4)">\
                                <ellipse id="s-Ellipse_4" class="ellipse shape non-processed-shape manualfit firer commentable non-processed" customid="Ellipse 5" cx="8.0" cy="8.181818181818187" rx="8.0" ry="8.181818181818187">\
                                </ellipse>\
                        </g>\
                    </g>\
                    <defs>\
                        <clipPath id="clip-s-Ellipse_4" class="clipPath">\
                                <ellipse cx="8.0" cy="8.181818181818187" rx="8.0" ry="8.181818181818187">\
                                </ellipse>\
                        </clipPath>\
                    </defs>\
                </svg>\
                <div class="paddingLayer">\
                    <div id="shapert-s-Ellipse_4" class="content firer" >\
                        <div class="valign">\
                            <span id="rtr-s-Ellipse_4_0"></span>\
                        </div>\
                    </div>\
                </div>\
            </div>\
            <div id="s-Paragraph_49" class="richtext autofit firer ie-background commentable non-processed" customid="Consumer-Base"   datasizewidth="38.3px" datasizeheight="19.0px" dataX="110.8" dataY="376.8" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_49_0">Terror</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
\
        </div>\
\
      </div>\
\
\
      <div id="s-Group_38" class="group firer ie-background commentable non-processed" customid="Searcher" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Input_1" class="text firer ie-background commentable non-processed" customid="Input 1"  datasizewidth="298.6px" datasizeheight="44.0px" dataX="1024.0" dataY="89.5" ><div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder="Buscar Libro"/></div></div>  </div></div></div>\
        <div id="s-Path_51" class="path firer commentable non-processed" customid="Search icon"   datasizewidth="15.2px" datasizeheight="15.2px" dataX="1288.6" dataY="103.5"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="15.177825927734375" height="15.177825927734375" viewBox="1288.556884765625 103.49997580621931 15.177825927734375 15.177825927734375" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_51-d7263" d="M1303.54877357218 117.59982136528654 L1299.899989408917 113.95103720202343 C1300.8044220579995 112.84828637481692 1301.3495059715376 111.43582402905784 1301.3495059715376 109.89628640917567 C1301.3495059715376 106.36516854075221 1298.4843127846718 103.49997580621931 1294.9531953685814 103.49997580621931 C1291.4190574987067 103.49997580621931 1288.556884765625 106.36516854075221 1288.556884765625 109.89628640917567 C1288.556884765625 113.42740382526611 1291.4190574987067 116.29259701213203 1294.9531953685814 116.29259701213203 C1296.4927329884636 116.29259701213203 1297.902020861043 115.75053377854456 1299.0047716882493 114.84610022479589 L1302.6535558515125 118.49186461277435 C1302.9014334453477 118.73978110724983 1303.3010506762398 118.73978110724983 1303.54877357218 118.49186461277435 C1303.7966900666554 118.24716149211886 1303.7966900666554 117.84452448594203 1303.54877357218 117.59982136528654 Z M1294.9531953685814 115.01959979442387 C1292.1252113226387 115.01959979442387 1289.826900769439 112.72125045366712 1289.826900769439 109.89628640917567 C1289.826900769439 107.07132236468422 1292.1252113226387 104.76999181003333 1294.9531953685814 104.76999181003333 C1297.778159413073 104.76999181003333 1300.0796832270094 107.07132236468422 1300.0796832270094 109.89628640917567 C1300.0796832270094 112.72125045366712 1297.778159413073 115.01959979442387 1294.9531953685814 115.01959979442387 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_51-d7263" fill="#2B3034" fill-opacity="0.4"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Paragraph_50" class="richtext manualfit firer ie-background commentable non-processed" customid="Dashboard"   datasizewidth="334.5px" datasizeheight="44.0px" dataX="57.0" dataY="109.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_50_0">Estadisticas de mi biblioteca</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Image_1" class="image firer click ie-background commentable non-processed" customid="Image 1"   datasizewidth="245.0px" datasizeheight="76.0px" dataX="50.1" dataY="13.5"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/06e16274-fae4-4c9b-b734-8bf0dc458157.png" />\
        	</div>\
        </div>\
      </div>\
\
      <div id="s-Table_1" class="table firer commentable non-processed" customid="Basic menu"  datasizewidth="1034.3px" datasizeheight="60.5px" dataX="347.0" dataY="21.2" originalwidth="1032.2506683349607px" originalheight="58.5px" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <table summary="">\
              <tbody>\
                <tr>\
                  <td id="s-Cell_9" customid="Cell 1" class="cellcontainer firer click ie-background non-processed"    datasizewidth="129.0px" datasizeheight="58.5px" dataX="0.0" dataY="0.0" originalwidth="129.03133354187008px" originalheight="58.5px" >\
                    <div class="cellContainerChild">\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                    	  <div class="layout scrollable">\
                    	    <div class="paddingLayer">\
                            <div class="center ghostHLayout">\
                            <table class="layout" summary="">\
                              <tr>\
                                <td class="layout horizontal insertionpoint verticalalign Cell_9 Table_1" valign="middle" align="center" hSpacing="6" vSpacing="0"><div id="s-Paragraph_51" class="richtext manualfit firer mouseenter mouseleave click ie-background commentable non-processed" customid="Inicio"   datasizewidth="40.0px" datasizeheight="19.0px" dataX="7.1" dataY="0.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_51_0">Inicio</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div></td> \
                              </tr>\
                            </table>\
                            </div>\
\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </td>\
                  <td id="s-Cell_10" customid="Cell 2" class="cellcontainer firer click ie-background non-processed"    datasizewidth="129.0px" datasizeheight="58.5px" dataX="0.0" dataY="0.0" originalwidth="129.03133354187008px" originalheight="58.5px" >\
                    <div class="cellContainerChild">\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                    	  <div class="layout scrollable">\
                    	    <div class="paddingLayer">\
                            <div class="center ghostHLayout">\
                            <table class="layout" summary="">\
                              <tr>\
                                <td class="layout horizontal insertionpoint verticalalign Cell_10 Table_1" valign="middle" align="center" hSpacing="6" vSpacing="0"><div id="s-Paragraph_52" class="richtext manualfit firer click ie-background commentable non-processed" customid="Registro"   datasizewidth="88.0px" datasizeheight="34.0px" dataX="9.0" dataY="-0.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_52_0">Registro</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div></td> \
                              </tr>\
                            </table>\
                            </div>\
\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </td>\
                  <td id="s-Cell_11" customid="Cell 3" class="cellcontainer firer click ie-background non-processed"    datasizewidth="129.0px" datasizeheight="58.5px" dataX="0.0" dataY="0.0" originalwidth="129.03133354187008px" originalheight="58.5px" >\
                    <div class="cellContainerChild">\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                    	  <div class="layout scrollable">\
                    	    <div class="paddingLayer">\
                            <div class="center ghostHLayout">\
                            <table class="layout" summary="">\
                              <tr>\
                                <td class="layout horizontal insertionpoint verticalalign Cell_11 Table_1" valign="middle" align="center" hSpacing="6" vSpacing="0"><div id="s-Paragraph_53" class="richtext manualfit firer click ie-background commentable non-processed" customid="Ingreso"   datasizewidth="67.0px" datasizeheight="34.0px" dataX="13.5" dataY="-0.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_53_0">Ingreso</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div></td> \
                              </tr>\
                            </table>\
                            </div>\
\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </td>\
                  <td id="s-Cell_12" customid="Cell 4" class="cellcontainer firer click ie-background non-processed"    datasizewidth="129.0px" datasizeheight="58.5px" dataX="0.0" dataY="0.0" originalwidth="129.03133354187008px" originalheight="58.5px" >\
                    <div class="cellContainerChild">\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                    	  <div class="layout scrollable">\
                    	    <div class="paddingLayer">\
                            <div class="center ghostHLayout">\
                            <table class="layout" summary="">\
                              <tr>\
                                <td class="layout horizontal insertionpoint verticalalign Cell_12 Table_1" valign="middle" align="center" hSpacing="6" vSpacing="0"><div id="s-Paragraph_54" class="richtext manualfit firer click ie-background commentable non-processed" customid="Tienda"   datasizewidth="77.0px" datasizeheight="34.0px" dataX="18.5" dataY="-0.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_54_0">Tienda</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div></td> \
                              </tr>\
                            </table>\
                            </div>\
\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </td>\
                  <td id="s-Cell_13" customid="Cell 5" class="cellcontainer firer click ie-background non-processed"    datasizewidth="129.0px" datasizeheight="58.5px" dataX="0.0" dataY="0.0" originalwidth="129.03133354187008px" originalheight="58.5px" >\
                    <div class="cellContainerChild">\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                    	  <div class="layout scrollable">\
                    	    <div class="paddingLayer">\
                            <div class="center ghostHLayout">\
                            <table class="layout" summary="">\
                              <tr>\
                                <td class="layout horizontal insertionpoint verticalalign Cell_13 Table_1" valign="middle" align="center" hSpacing="6" vSpacing="0"><div id="s-Paragraph_55" class="richtext manualfit firer click ie-background commentable non-processed" customid="Estadisticas"   datasizewidth="89.0px" datasizeheight="34.0px" dataX="11.0" dataY="0.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_55_0">Estadisticas</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div></td> \
                              </tr>\
                            </table>\
                            </div>\
\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </td>\
                  <td id="s-Cell_14" customid="Cell 6" class="cellcontainer firer ie-background non-processed"    datasizewidth="129.0px" datasizeheight="58.5px" dataX="0.0" dataY="0.0" originalwidth="129.03133354187008px" originalheight="58.5px" >\
                    <div class="cellContainerChild">\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                    	  <div class="layout scrollable">\
                    	    <div class="paddingLayer">\
                            <div class="center ghostHLayout">\
                            <table class="layout" summary="">\
                              <tr>\
                                <td class="layout horizontal insertionpoint verticalalign Cell_14 Table_1" valign="middle" align="center" hSpacing="6" vSpacing="0"><div id="s-Paragraph_56" class="richtext manualfit firer ie-background commentable non-processed" customid="Aliados"   datasizewidth="58.0px" datasizeheight="34.0px" dataX="9.0" dataY="-0.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_56_0">Aliados</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div></td> \
                              </tr>\
                            </table>\
                            </div>\
\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </td>\
                  <td id="s-Cell_15" customid="Cell 7" class="cellcontainer firer ie-background non-processed"    datasizewidth="129.0px" datasizeheight="58.5px" dataX="0.0" dataY="0.0" originalwidth="129.03133354187008px" originalheight="58.5px" >\
                    <div class="cellContainerChild">\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                    	  <div class="layout scrollable">\
                    	    <div class="paddingLayer">\
                            <div class="center ghostHLayout">\
                            <table class="layout" summary="">\
                              <tr>\
                                <td class="layout horizontal insertionpoint verticalalign Cell_15 Table_1" valign="middle" align="center" hSpacing="6" vSpacing="0"><div id="s-Paragraph_57" class="richtext manualfit firer ie-background commentable non-processed" customid="Facebook"   datasizewidth="96.0px" datasizeheight="34.0px" dataX="11.5" dataY="0.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_57_0">Facebook</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div></td> \
                              </tr>\
                            </table>\
                            </div>\
\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </td>\
                  <td id="s-Cell_16" customid="Cell 8" class="cellcontainer firer ie-background non-processed"    datasizewidth="129.0px" datasizeheight="58.5px" dataX="0.0" dataY="0.0" originalwidth="129.03133354187008px" originalheight="58.5px" >\
                    <div class="cellContainerChild">\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                    	  <div class="layout scrollable">\
                    	    <div class="paddingLayer">\
                            <div class="center ghostHLayout">\
                            <table class="layout" summary="">\
                              <tr>\
                                <td class="layout horizontal insertionpoint verticalalign Cell_16 Table_1" valign="middle" align="center" hSpacing="6" vSpacing="0"><div id="s-Paragraph_58" class="richtext manualfit firer ie-background commentable non-processed" customid="Whatsapp"   datasizewidth="71.0px" datasizeheight="34.0px" dataX="15.5" dataY="-0.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-s-Paragraph_58_0">Whatsapp</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div></td> \
                              </tr>\
                            </table>\
                            </div>\
\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </td>\
                </tr>\
              </tbody>\
            </table>\
          </div>\
        </div>\
      </div>\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;